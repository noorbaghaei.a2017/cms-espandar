@extends('template.app')


@section('content')




  <!-- content-->
  <div class="content">

  @include('template.auth.header-welcome')

                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">

                            @include('template.alert.error')
                            @include('template.alert.success')

@foreach ($menus as $menu)
      <!-- dashboard-list --> 
      <div class="dashboard-title  dt-inbox fl-wrap"> 
      <h3>{{$menu->title}}</h3>  
      </div>
                                 @foreach ($item->menus->where('menu',$menu->id) as $value)
                                 <div class="dashboard-list fl-wrap">
                                        <div class="dashboard-message">
                                           
                                            <div class="booking-list-contr">
                                                
                                               
                                                    
                                                     <a href="{{route('edit.menu.job',['token'=>$value->token])}}" class="color-bg tolt" data-microtip-position="left" data-tooltip="{{__('cms.edit')}}"><i class="fal fa-edit"></i></a>
                                                    <a href="{{route('remove.menu.job',['token'=>$value->token])}}" class="color-bg tolt" data-microtip-position="left" data-tooltip="{{__('cms.remove')}}"><i class="fal fa-trash"></i></a>
                         

                                               

                                            </div>
                                           
                                            <div class="dashboard-message-text">

                                            <div style="text-align:left;padding-bottom:20px;">
                                            <a href="{{route('client.menu.job.language.show',['lang'=>'fa','token'=>$value->token])}}"><img src="{{asset('img/flag/iran.png')}}" style="width:30px;{{checkLangTranslate('menujob','en',$value->id) ? '' : 'filter: grayscale(100%);' }}"></a>
                                                <a href="{{route('client.menu.job.language.show',['lang'=>'en','token'=>$value->token])}}"><img src="{{asset('img/flag/united-kingdom.png')}}" style="width:30px;{{checkLangTranslate('menujob','en',$value->id) ? '' : 'filter: grayscale(100%);' }}"></a>
                                            
                                            </div>
                                            <br>
                                                          
                                                            @if(!$value->Hasmedia('images'))
                                                                   <img  src="{{asset('img/no-img.gif')}}" alt="title" title="title" >
                                                               @else
                                                               <img  src="{{$value->getFirstMediaUrl('images')}}" alt="title" title="title" >
                                                               @endif                                                              
                                               
                                                <h4>
                                                    <a href="#">{{$value->title}} / {{$value->excerpt}} </a>
                                                   
                                                </h4>
                  
                                                <div class="geodir-category-location clearfix"><a href="#"> {{$value->price}} </a></div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- dashboard-list end-->  
                                     
                                 @endforeach
                                  <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">
                                <form method="POST" action="{{route('add.menu.job',['token'=>$item->token,'menu'=>$menu->id])}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="custom-form" >
                                        <div class="row">
                                         <!--col --> 
                                         <div class="col-md-4">
                                               
                                               
                                                   
                                         <label> {{__('cms.title')}} <i class="fal fa-user"></i></label>
                                        <input type="text" class="{{$errors->has('title') ? 'error-input' : ''}}" name="title" placeholder="" value=""/>
                                        

                                                      


                                                 
                                               
                                           </div>
                                           <!--col end--> 
                                           
                                                      <!--col --> 
                                         <div class="col-md-4">
                                               
                                               
                                                   
                                               <label> {{__('cms.price')}} <i class="fas fa-euro-sign"></i></label>
                                              <input class="{{$errors->has('price') ? 'error-input' : ''}}" type="text" name="price" placeholder="" value=""/>
                                              
      
                                                            
      
      
                                                       
                                                     
                                                 </div>
                                                 <!--col end--> 
                                            <!--col --> 
                                            <div class="col-md-4">
                                            <div>
                        <p style="color:red">image-size : 468px * 376px</p>
                        </div>
                                                <div class="add-list-media-wrap">
                                                   
                                                    
                                                        <div id="bg-{{$menu->id}}" class="fuzone">
                                                            <div id="text-image-{{$menu->id}}" class="fu-text">
                                                                <span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
                                                            </div>
                                                                <input type="file" name="image-{{$menu->id}}" class="upload" onchange="loadFile(event,{{$menu->id}})">
                                                       

                                                        </div>


                                                  
                                                </div>
                                            </div>
                                            <!--col end--> 
                                            
                                           
                                                                                          
                                        </div>
                                        <div class="row">
  <!--col --> 
  <div class="col-md-12">
                                               
                                               
                                                   
                                               <label> {{__('cms.excerpt')}} <i class="fal fa-text"></i></label>
                                              <input class="{{$errors->has('excerpt') ? 'error-input' : ''}}" type="text" name="excerpt" placeholder="" value=""/>
                                              
      
                                                            
      
      
                                                       
                                                     
                                                 </div>
                                                 <!--col end--> 
                                        </div>
                                    </div>
                                    <button type="submit" class="logout_btn color2-bg">{{__('cms.save')}} <i class="fas fa-sign-out"></i></button>

                                    </form>

                                </div>
                                <!-- profile-edit-container end-->

@endforeach
                                  
                            
                                                  
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection



@section('scripts')


<script>

function loadFile(e,id) {
	 image = $('#bg-'+id);
     text = $('#text-image-'+id);
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:cover');
    
};

            function loadCity() {



                    $code=$("#postal_code").val();
                   
               
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                    contentType:'application/json; charset=utf-8',
                    url:'/ajax/load/city/postalcode/'+$code,
                    data: { field1:$code} ,
                    beforeSend:function(){
                    
                },
                    success: function (response) {
                       if(response.data.status){

                           console.log(response.data);
                           $('#city').empty();
                           $('.nice-select .list').empty();
                           $.each(response.data.result, function(index, key) {
                         
                            $('#city').append("<option value='"+key.fields.plz_name+"-"+key.fields.krs_name+"'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</option>");
                            });
                            $.each(response.data.result, function(index, key) {
                               
                                $('.nice-select .list').append("<li data-value='"+key.fields.plz_name+"-"+key.fields.krs_name+"' class='option'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</li>");
                            });
                              

                              

                       }
                       else{
                        console.log('no');
                       }
                    },
                    error: function (xhr,ajaxOptions,thrownError) {
                    
                    console.log(xhr,ajaxOptions,thrownError);
                        // $("#result-ajax").empty()
                        //     .append("<span>"+msg+"</span>");
                    },
                    complete:function(){
                   
                }

                });

           }

</script>


@endsection




