@extends('template.app')


@section('content')




  <!-- content-->
  <div class="content">
  @include('template.auth.header-welcome')
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">
                            @include('template.alert.success')
                            @include('template.alert.error')
                           
                             <!-- dashboard-list-box--> 
                                 <div class="dashboard-list-box  fl-wrap" >
                                 @foreach ($items as $question)
                                     
                                 <!-- dashboard-list -->    
                                 <div class="dashboard-list jobs fl-wrap">
                                        <div class="dashboard-message">
                                           
                                            <div class="booking-list-contr">
                                          
                                                     <a href="{{route('client.show.edit.question.werbung',['question'=>$question->token])}}" class="color-bg tolt" data-microtip-position="left" data-tooltip="{{__('cms.edit')}}"><i class="fal fa-edit"></i></a>
                                                 

                                            </div>
                                            
                                            <div class="dashboard-message-text">
                                            <div class="option_button">
                                            
                                                <a class="translate_buttom btn btn-primary"  style="background: #4DB7FE !important;"  href="{{setLangJob(LaravelLocalization::getCurrentLocale(),'fa',route('client.werbung.question.language.show',['lang'=>'fa','token'=>$question->token]))}}">{{__('cms.translate_persion')}}</a>
                                                <a class="translate_buttom btn btn-primary"   style="background: #4DB7FE !important;"  href="{{setLangJob(LaravelLocalization::getCurrentLocale(),'en',route('client.werbung.question.language.show',['lang'=>'en','token'=>$question->token]))}}">{{__('cms.translate_english')}}</a>
                                            
                                            </div>
                                            <br>
                                                          

                                               
                                                <h4>
                                                
                                                </h4>
                                                <div class="geodir-category-location clearfix"><a href="#"> {{convert_lang($question,LaravelLocalization::getCurrentLocale(),'title')}} </a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- dashboard-list end-->   
                                 @endforeach

                                    
                            <form action="{{route('store.question.client.werbung',['werbung'=>$item->token])}}" method="POST" enctype="multipart/form-data" >
                              @csrf

      

                                <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">
                                <div style="margin-bottom:10px;"></div>
                                <div class="custom-form">

    <div class="row">
            <div class="col-sm-4">
                    <label >{{__('cms.title')}} * <i class="fal fa-user"></i></label>
                    <input type="text" name="title" class="{{$errors->has('title') ? 'error-input' : ''}}" placeholder="{{__('cms.title')}}" value="{{old('title')}}" autocomplete="off"/>                                                
             </div>
             <div class="col-sm-12" style="margin-bottom:20px;margin-top:20px">
                                                <label>{{__('cms.text')}}  <i class="fal fa-text"></i></label>
                                               
                                                <textarea name="text" cols="40" rows="3" placeholder="{{__('cms.text')}}">{{old('text') ? old('text') : ''}}</textarea> 
                                            
                                            </div>
    </div>

    </div>
   

          
</div>

<button type="submit" class="logout_btn color2-bg">{{__('cms.save')}} <i class="fas fa-sign-out"></i></button>


                            </form>           
                                           


   

                                     
                                </div>
                                <!-- dashboard-list-box end--> 
                           
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection



@section('scripts')


<script>

            function loadCity() {



                    $code=$("#postal_code").val();
                   
               
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                    contentType:'application/json; charset=utf-8',
                    url:'/ajax/load/city/postalcode/'+$code,
                    data: { field1:$code} ,
                    beforeSend:function(){
                    
                },
                    success: function (response) {
                       if(response.data.status){

                           console.log(response.data);
                           $('#city').empty();
                           $('.nice-select .list').empty();
                           $.each(response.data.result, function(index, key) {
                         
                            $('#city').append("<option value='"+key.fields.plz_name+"-"+key.fields.krs_name+"'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</option>");
                            });
                            $.each(response.data.result, function(index, key) {
                               
                                $('.nice-select .list').append("<li data-value='"+key.fields.plz_name+"-"+key.fields.krs_name+"' class='option'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</li>");
                            });
                              

                              

                       }
                       else{
                        console.log('no');
                       }
                    },
                    error: function (xhr,ajaxOptions,thrownError) {
                    
                    console.log(xhr,ajaxOptions,thrownError);
                        // $("#result-ajax").empty()
                        //     .append("<span>"+msg+"</span>");
                    },
                    complete:function(){
                   
                }

                });

           }

</script>


@endsection




