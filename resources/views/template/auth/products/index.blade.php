@extends('template.app')


@section('content')




  <!-- content-->
  <div class="content">
  @include('template.auth.header-welcome')
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">
                            @include('template.alert.success')
                           
                             <!-- dashboard-list-box--> 
                                 <div class="dashboard-list-box  fl-wrap">
                                 @foreach ($client->products as $product)
                                     
                                 <!-- dashboard-list -->    
                                 <div class="dashboard-list jobs fl-wrap">
                                        <div class="dashboard-message">
                                           
                                            <div class="booking-list-contr">
                                          
                                            <a href="{{route('client.show.edit.product',['product'=>$product->token])}}" class="color-bg tolt" data-microtip-position="left" data-tooltip="{{__('cms.edit')}}"><i class="fal fa-edit"></i></a>
                                            <a href="{{route('client.show.comments.product',['product'=>$product->token])}}" class="color-bg tolt" data-microtip-position="left" data-tooltip="{{__('cms.comment')}}"><i class="fal fa-envelope"></i></a>
                                                    <a href="{{route('client.show.galleries.product',['product'=>$product->token])}}" class="color-bg tolt" data-microtip-position="left" data-tooltip="{{__('cms.galleries')}}"><i class="fal fa-image"></i></a>
                                                    
                                            </div>
                                            
                                            <div class="dashboard-message-text">
                                            <div class="option_button">
                                            
                                                <a class="translate_buttom btn btn-primary"  style="background: #4DB7FE !important;"  href="{{setLangJob(LaravelLocalization::getCurrentLocale(),'fa',route('client.product.language.show',['lang'=>'fa','token'=>$product->token]))}}">{{__('cms.translate_persion')}}</a>
                                                <a class="translate_buttom btn btn-primary"   style="background: #4DB7FE !important;"  href="{{setLangJob(LaravelLocalization::getCurrentLocale(),'en',route('client.product.language.show',['lang'=>'en','token'=>$product->token]))}}">{{__('cms.translate_english')}}</a>
                                            
                                            </div>
                                            <br>
                                                            @if(!$product->Hasmedia('images'))
                                                                   <img  src="{{asset('template/images/no-image.jpg')}}" alt="title" title="title" >
                                                               @else
                                                               <img  src="{{$product->getFirstMediaUrl('images')}}" alt="title" title="title" >
                                                               @endif
                                               
                                                <h4>
                                                     @if($product->status==2)
                                                    <a href="{{route('bussiness.single',['bussiness'=>$product->slug])}}">{{convert_lang($product,LaravelLocalization::getCurrentLocale(),'title')}} -  </a><span>{{statusTitleProduct($product->status)}}</span>
                                                @else
                                                <a href="#">{{convert_lang($product,LaravelLocalization::getCurrentLocale(),'title')}} -  </a><span>{{statusTitleProduct($product->status)}}</span>

                                                @endif
                                                </h4>
                                                <div class="geodir-category-location clearfix"><a href="#"> {{$product->city}} </a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- dashboard-list end-->   
                                 @endforeach
                                     
                                                                           
                                </div>
                                <!-- dashboard-list-box end--> 
                           
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection



@section('scripts')


<script>

            function loadCity() {



                    $code=$("#postal_code").val();
                   
               
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                    contentType:'application/json; charset=utf-8',
                    url:'/ajax/load/city/postalcode/'+$code,
                    data: { field1:$code} ,
                    beforeSend:function(){
                    
                },
                    success: function (response) {
                       if(response.data.status){

                           console.log(response.data);
                           $('#city').empty();
                           $('.nice-select .list').empty();
                           $.each(response.data.result, function(index, key) {
                         
                            $('#city').append("<option value='"+key.fields.plz_name+"-"+key.fields.krs_name+"'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</option>");
                            });
                            $.each(response.data.result, function(index, key) {
                               
                                $('.nice-select .list').append("<li data-value='"+key.fields.plz_name+"-"+key.fields.krs_name+"' class='option'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</li>");
                            });
                              

                              

                       }
                       else{
                        console.log('no');
                       }
                    },
                    error: function (xhr,ajaxOptions,thrownError) {
                    
                    console.log(xhr,ajaxOptions,thrownError);
                        // $("#result-ajax").empty()
                        //     .append("<span>"+msg+"</span>");
                    },
                    complete:function(){
                   
                }

                });

           }

</script>


@endsection




