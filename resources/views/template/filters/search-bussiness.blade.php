     <!--tabs -->                       
     <div class="tabs-container fl-wrap  ">
                                    <!--tab -->
                                    <form action="{{route('search.page.bussiness',['filter'=>1])}}" method="GET">
                               
                                    <div class="tab">

                                        <div id="tab-inpt1" class="tab-content first-tab">
                                            <div class="main-search-input-wrap fl-wrap">
                                                <div class="main-search-input fl-wrap">
                                                     
                                                     <div class="main-search-input-item " >
                                                        <label><i class="fal fa-info"></i></label>
          
                                                   <input id="what" type="text"  onkeyup="loadCategory(event)"  placeholder=" {{__('cms.what')}} " value="" autocomplete="off"/>

                                                   <input id="secret_what"  name="was" value="" hidden>
                                                      

                                                     
                                                        <div id="result-was">

                                                            <ul id="item-was">
                                                          
                                                             
                                                            </ul>
                                                            <img id="item-was-loader" hidden src="{{asset('template/images/preloader-text.gif')}}">

                                                             
                                                        </div>

                                                    </div>
                                                    <div class="main-search-input-item " id="location-search">
                                                   
                                                    <a id="find_location" href="#" class="btn-gps"><i class="fal fa-dot-circle"></i></a>
                                                        <label><i class="fal fa-map-marker-alt"></i></label>
                                                       
                                                        <input id="wo" type="text" onkeyup="loadInfo(event)"  placeholder=" {{__('cms.where')}} " value="" autocomplete="off"/>
                                                        <input id="secret_wo"  name="wo" value="" hidden>

                                                       
                                                        <div id="result-wo">

                                                                <ul id="item-wo">

                                                                
                                                                </ul>
                                                                <img id="item-wo-loader"  hidden src="{{asset('template/images/preloader-text.gif')}}">

                                                                
                                                                </div>
                                                   
                                                    </div>
                                                   
                                                   
                                                   <input hidden id="longitude" name="longitude" type="text" value="">
                                                   <input hidden id="latitude" name="latitude" type="text" value="">  
                                                   
                                                    <button type="submit" class="main-search-button color2-bg" >{{__('cms.search')}} <i class="far fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                    <!--tab end-->