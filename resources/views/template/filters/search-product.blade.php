
                                    <form action="{{route('search.page.product',['filter'=>1])}}" method="GET">
                               
                               <!--tab -->
                               <div class="tab">
                               <div id="tab-inpt2" class="tab-content">
                                   <div class="main-search-input-wrap fl-wrap">
                                       <div class="main-search-input fl-wrap">
                                       <div class="main-search-input-item product-width" >
                                               <label><i class="fal fa-info"></i></label>
 
                                          <input id="what_product" type="text" onkeyup="loadCategoryProduct(event)"    placeholder=" {{__('cms.what')}} " value="" autocomplete="off"/>

                                          <input id="secret_what_product"  name="was_product" value="" hidden>
                                             

                                            
                                               <div id="result-was-product">

                                                   <ul id="item-was-product">
                                                 
                                                    
                                                   </ul>
                                                   <img id="item-was-loader-product"  hidden src="{{asset('template/images/preloader-text.gif')}}">

                                                    
                                               </div>

                                           </div>
                                           <div class="main-search-input-item product-width " >
                                                        <label><i class="fas fa-euro-sign"></i></label>
                                                       
 
                                          <input id="price_product" name="price_product" type="text"  placeholder=" {{__('cms.price')}} " value="" autocomplete="off"/>

                                        
                            
                                           </div>
                                           <div class="main-search-input-item product-width " >
                                           <a id="find_location_product" href="#" class="btn-gps"><i class="fal fa-dot-circle"></i></a>
                                                        <label><i class="fal fa-map-marker-alt"></i></label>
                                                       
 
                                          <input id="wo_product" type="text" onkeyup="loadInfoProduct(event)"    placeholder=" {{__('cms.where')}} " value="" autocomplete="off"/>

                                          <input id="secret_what_product"  name="wo_product" value="" hidden>
                                             

                                            
                                               <div id="result-wo-product">

                                                   <ul id="item-wo-product">
                                                 
                                                    
                                                   </ul>
                                                   <img id="item-wo-loader-product"  hidden src="{{asset('template/images/preloader-text.gif')}}">

                                                    
                                               </div>

                                           </div>
                                        
                                           <button type="submit" class="main-search-button color2-bg" >{{__('cms.search')}} <i class="far fa-search"></i></button>

                                           
                                        </div>
                                   </div>
                               </div>
                           </div>

                           </form>
                           <!--tab end-->