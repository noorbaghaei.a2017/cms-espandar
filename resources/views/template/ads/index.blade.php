@extends('template.app')

@section('content')

  <!-- content-->
  <div class="content">
                    <!--  section  -->
                    <section class="parallax-section single-par" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/bg/5.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <div class="container">
                            <div class="section-title center-align big-title">
                                
                             
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a> 
                        </div>
                    </section>
                    <!--  section  end-->
                    <section class="gray-bg small-padding no-top-padding-sec" id="sec1">
                        <div class="container">
                            <div class="breadcrumbs inline-breadcrumbs fl-wrap block-breadcrumbs">
                               
                            </div>
                            <div class="mob-nav-content-btn  color2-bg show-list-wrap-search ntm fl-wrap"><i class="fal fa-filter"></i>  Filters</div>
                            <div class="fl-wrap">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class=" fl-wrap lws_mobile">
                                           
                                                                                      
                                            <!--box-widget-item -->
                                            <div class="box-widget-item fl-wrap block_box">
                                                <div class="box-widget-item-header">
                                                    <h3>Kleinanzeige Tags</h3>
                                                </div>
                                                <div class="box-widget fl-wrap">
                                                    <div class="box-widget-content">
                                                        <div class="list-single-tags tags-stylwrap">
                                                                                                                                
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--box-widget-item end -->                                            
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                                            
                                        <!-- listing-item-container -->
                                        <div class="listing-item-container init-grid-items fl-wrap nocolumn-lic">
                                                                          
                                          
                                        </div>
                                        <!-- listing-item-container end -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--section end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection

