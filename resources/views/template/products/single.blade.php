@extends('template.app')

@section('content')

  <!-- content-->
  <div class="content">

                <!--section-->
                <section class="gray-bg small-padding no-top-padding-sec" id="sec1">
                    <div class="container">
                        <div class="breadcrumbs inline-breadcrumbs fl-wrap block-breadcrumbs">
                        </div>
                        <div class="share-holder hid-share sing-page-share top_sing-page-share">
                            <div class="share-container  isShare"></div>
                        </div>
                        <div class="mob-nav-content-btn  color2-bg show-list-wrap-search ntm fl-wrap"><i class="fal fa-filter"></i>  {{__('cms.filter')}}</div>
                        <div class="fl-wrap">
                            <div class="row">
                              @include('template.products.sections.sidebar-single')
                                <div class="col-md-8">
                                    <div class="fl-wrap block_box product-header">
                                        <div class="product-header-details">
                                           
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="single-slider-wrap shop-media-img">
                                                        <div class="single-slider fl-wrap">
                                                            <div class="swiper-container">
                                                                <div class="swiper-wrapper lightgallery">
                                                                @if($item->Hasmedia('images'))
                                                                 
                                                                   <div class="swiper-slide hov_zoom"><img src="{{$item->getFirstMediaUrl('images')}}" alt=""><a href="{{$item->getFirstMediaUrl('images')}}" class="box-media-zoom   popup-image"><i class="fal fa-search"></i></a></div>

                                                                 
                                                                    @else
                                                                    <div class="swiper-slide hov_zoom"><img src="{{asset('template/images/no-image.jpg')}}" alt=""><a href="{{asset('template/images/no-image.jpg')}}" class="box-media-zoom   popup-image"><i class="fal fa-search"></i></a></div>

                                                             

                                                               @endif
                                                               @foreach ($medias as  $media)
                                                               <div class="swiper-slide hov_zoom"><img src="/media/{{$media->id}}/{{$media->file_name}}" alt=""><a href="/media/{{$media->id}}/{{$media->file_name}}" class="box-media-zoom   popup-image"><i class="fal fa-search"></i></a></div>

                                                           
                                                               @endforeach   
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="listing-carousel_pagination">
                                                            <div class="listing-carousel_pagination-wrap">
                                                                <div class="ss-slider-pagination"></div>
                                                            </div>
                                                        </div>
                                                        @if(count($medias) > 0)
                                                        <div class="ss-slider-cont ss-slider-cont-prev color2-bg"><i class="fal fa-long-arrow-left"></i></div>
                                                        <div class="ss-slider-cont ss-slider-cont-next color2-bg"><i class="fal fa-long-arrow-right"></i></div>
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <h3>{{$item->title}} </h3>
                                                  
                                                    
                                                    <span class="product-header-details_price">{{$item->price->amount}}€ </span>
                                                   
                                                  
                                                    <span class="fw-separator"></span>
                                                    <div class="clearfix"></div>
<p>{{$item->excerpt}}</p>
@if(!is_null($item->mobile))
                                                    <div class="clearfix"></div>
                                            <p>{{__('cms.mobile')}} : <a id="telephone" href="tel::{{substr($item->mobile, 0,2)}}*****" >{{substr($item->mobile, 0,2)}}*****</a></p>
                                                    @endif
                                                    <span class="fw-separator"></span>
                                                    <div class="custom-form product-header_form ">
                                                 
                                                    @if(!is_null($item->mobile))
                                                        <button class="color-bg" id="request_mobile">{{__('cms.call')}}</button>
                                                        @endif
                                                     
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="fw-separator"></span>  
                                            <div class="list-single-tags tags-stylwrap">
                                                <span class="tags-title"><i class="fas fa-tag"></i> {{__('cms.tags')}} : </span>
                                                <a href="#">{{$item->title}}</a>
                                                                                                                         
                                            </div>
                                        </div>
                                    </div>
                                    <!-- shop-tabs--> 
                                    <div class="tabs-act fl-wrap">
                                        <div class="shop-tabs-menu " id="st-menu">
                                            <ul class="tabs-menu fl-wrap no-list-style">
                                                <li class="current"><a href="#shop-tab1"> {{__('cms.description')}}</a></li>
                                                <li><a href="#shop-tab2">{{__('cms.info')}} </a></li>
                                                <li><a href="#shop-tab3">{{__('cms.comment_item')}}</a></li>
                                                <li><a href="#shop-tab4">{{__('cms.question_item')}}</a></li>
                                            </ul>
                                        </div>
                                        <!-- shop-tabs--> 
                                        <!-- shop-tabs--> 
                                        <div class="shop-tabs fl-wrap block_box">
                                            <!--tabs -->                       
                                            <div class="tabs-container fl-wrap">
                                                <!--tab -->
                                                <div class="tab">
                                                    <div id="shop-tab1" class="tab-content  first-tab ">
                                                        <div class="shop-tab-container">
<p>{{$item->text}}</p>                                                        </div>
                                                    </div>
                                                </div>
                                                <!--tab end-->
                                                <!--tab --> 
                                                <div class="tab">
                                                    <div id="shop-tab2" class="tab-content">
                                                        <div class="shop-tab-container">
                                                            <ul class="no-list-style shop-list fl-wrap">
                                                                <li><span> {{__('cms.code')}} :</span>{{$item->code}}</li>
                                                              
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--tab end-->
                                                <!--tab --> 
                                                <div class="tab">
                                                    <div id="shop-tab3" class="tab-content">
                                                        <div class="shop-tab-container">
                                                            <div class="reviews-comments-wrap">
                                                                <!-- reviews-comments-item -->  
                                                                <div class="reviews-comments-item">
                                                                    <div class="review-comments-avatar">
                                                                        <img src="{{asset('template/images/avatar/4.jpg')}}" alt=""> 
                                                                    </div>
                                                                    <div class="reviews-comments-item-text fl-wrap">
                                                                        <div class="reviews-comments-header fl-wrap">
                                                                            <h4><a href="#"> test</a></h4>
                                                                          
                                                                        </div>
                                                                        <p>text</p>
                                                                        <div class="reviews-comments-item-footer fl-wrap">
                                                                            <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>2 days ago</span></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--reviews-comments-item end--> 
                                                                                                                             
                                                            </div>
                                                        </div>
                                                        <span class="fw-separator"></span>                                                       
                                                        <!-- Add Review Box -->
                                                        <div id="add-review" class="shop-review-box fl-wrap">
                                                            <!-- Review Comment -->
                                                            <div class="leave-rating-wrap">
                                                                <span class="leave-rating-title">{{__('cms.comment_item')}}: </span>
                                                              
                                                            </div>
                                                            <form id="add-comment" class="add-comment  custom-form" name="rangeCalc" >
                                                                <fieldset>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label><i class="fal fa-user"></i></label>
                                                                            <input type="text" placeholder="{{__('cms.title')}} *" value=""/>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label><i class="fal fa-envelope"></i>  </label>
                                                                            <input type="text" placeholder="{{__('cms.email')}}*" value=""/>
                                                                        </div>
                                                                    </div>
                                                                    <textarea cols="40" rows="3" placeholder="{{__('cms.message')}}:"></textarea>
                                                                    <div class="clearfix"></div>
                                                                    <button class="btn  color2-bg  float-btn">{{__('cms.send')}} <i class="fal fa-paper-plane"></i></button>
                                                                </fieldset>
                                                            </form>
                                                        </div>
                                                        <!-- Add Review Box / End -->                                                            
                                                    </div>
                                                </div>
                                            </div>
                                            <!--tab end-->    
                                            <!--tab --> 
                                            <div class="tab">
                                                    <div id="shop-tab4" class="tab-content">
                                                        <div class="shop-tab-container">
                                                           
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--tab end-->                                                
                                        </div>
                                        <!--tabs end-->                                      	
                                    </div>
                                    <!-- shop-tabs end--> 
                                    <!-- list-single-main-item --> 
                                    <div class="list-single-main-item fl-wrap block_box">
                                        <div class="list-single-main-item-title">
                                            <h3> {{__('cms.popular-product')}}</h3>
                                        </div>
                                        <div class="list-single-main-item_content  fl-wrap">

                                        @foreach ($populars as $popular)
                                            
                                       
                                            <!-- shop-item  -->
                                            <div class="shop-item">
                                                <div class="shop-item-media">
                                                    <a href="{{route('products.single',['product'=>$popular->slug])}}">
                                                    @if($popular->Hasmedia('images'))
                                                                 
                                                                 <img src="{{$popular->getFirstMediaUrl('images')}}" alt="">
                                                               
                                                                  @else
                                                                  <img src="{{asset('template/images/no-image.jpg')}}" alt="">
                                                           

                                                             @endif
                                                       
                                                        <div class="overlay"></div>
                                                    </a>
                                                  
                                                </div>
                                                <div class="shop-item_title">
                                                    <h4><a href="{{route('products.single',['product'=>$popular->slug])}}">{{$popular->title}}</a></h4>
                                                    <span class="shop-item_price">{{$popular->price->amount}}€</span>
                                                    <a href="{{route('products.single',['product'=>$popular->slug])}}" class="shop-item_link color-bg">{{__('cms.read_more')}}</a>
                                                </div>
                                            </div>
                                            <!-- shop-item end -->     
                                            @endforeach                               
                                        </div>
                                    </div>
                                    <!-- list-single-main-item end -->                                     
                                </div>
                            </div>
                        </div>
                </section>
                <!--section end-->
                <div class="limit-box fl-wrap"></div>


@endsection


@section('scripts')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<script src="{{asset('template/js/shop.js')}}" defer></script>
<link type="text/css" rel="stylesheet" href="{{asset('template/css/shop.css?')}}{{uniqid()}}">

@else

<script src="{{asset('template/js/ltr-shop.js')}}" defer></script>
<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-shop.css?')}}{{uniqid()}}">


@endif

<script>


$('#request_mobile').on('click',function(e){

$mobile="{{$item->mobile}}";
$(this).hide();
$("#telephone").text($mobile);
$("#telephone").attr('href',"tel:"+$mobile);

});


</script>


@endsection

