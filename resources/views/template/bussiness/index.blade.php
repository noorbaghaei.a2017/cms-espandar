@extends('template.app')

@section('content')

  <!-- content-->
  <div class="content">
                    <!--  section  -->
                    <section class="parallax-section single-par" data-scrollax-parent="true">
                   
                   
                    <div class="bg par-elem "  data-bg="{{asset('template/images/banner_category.jpeg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                  
                     
                        <div class="overlay op7"></div>
                        <div class="container">
                            <div class="section-title center-align big-title">
                                
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a> 
                        </div>
                    </section>
                    <!--  section  end-->
                    <section class="gray-bg small-padding no-top-padding-sec" id="sec1">
                        <div class="container">
                        @if(!isset($queries))

                           
<div class="breadcrumbs inline-breadcrumbs fl-wrap block-breadcrumbs">
<a href="{{route('front.website')}}">{{__('cms.home')}}</a>
@if(isset($category))
@if(showServiceCategory($category)->parent!='0')
<a href="{{route('search.page.bussiness',['filter'=>1,'category'=>showChildCategoryJob($category)->id])}}">{{convert_lang(showChildCategoryJob($category),LaravelLocalization::getCurrentLocale(),'title')}}</a> 
@endif
<span> {{convert_lang(showServiceCategory($category),LaravelLocalization::getCurrentLocale(),'title')}}</span> 

</div>
@else

@endif

@endif
                            <!-- list-main-wrap-header-->
                            <div class="list-main-wrap-header fl-wrap   block_box no-vis-shadow no-bg-header fixed-listing-header">
                                <!-- list-main-wrap-title-->
                               
                                <!-- list-main-wrap-title end-->
                                <!-- list-main-wrap-opt-->
                                <div class="list-main-wrap-opt">
                                    
                                    <!-- price-opt-->
                                    <div class="grid-opt">
                                        <ul class="no-list-style">
                                            <li class="grid-opt_act"><span class="two-col-grid act-grid-opt tolt" data-microtip-position="bottom" data-tooltip="نمایش شبکه ای"><i class="fal fa-th"></i></span></li>
                                            <li class="grid-opt_act"><span class="one-col-grid tolt" data-microtip-position="bottom" data-tooltip="نمایش لیستی"><i class="fal fa-list"></i></span></li>
                                        </ul>
                                    </div>
                                    <!-- price-opt end-->
                                </div>
                                <!-- list-main-wrap-opt end-->                    
                                <a class="custom-scroll-link back-to-filters clbtg" href="#lisfw"><i class="fal fa-search"></i></a>
                            </div>
                            <!-- list-main-wrap-header end-->
                            @if($filter)                      
                            <div class="mob-nav-content-btn  color2-bg show-list-wrap-search ntm fl-wrap"><i class="fal fa-filter"></i>  {{__('cms.filter')}}</div>
                            @endif
                            <div class="fl-wrap">

                            @if($filter)
                                <!-- listsearch-input-wrap-->
                                <div class="listsearch-input-wrap lws_mobile fl-wrap tabs-act inline-lsiw" id="lisfw">
                                    <div class="listsearch-input-wrap_contrl fl-wrap">
                                        <ul class="tabs-menu fl-wrap no-list-style">
                                            <li class="current"><a href="#filters-search"> <i class="fal fa-sliders-h"></i> {{__('cms.filter')}} </a></li>
                                            <li><a href="#category-search"> <i class="fal fa-image"></i>  {{__('cms.category')}} </a></li>
                                        </ul>
                                    </div>
                                  
                                    <!--tabs -->                       
                                    <div class="tabs-container fl-wrap">
                       
                                        @if(count($queries) > 0)
                                        @include('template.bussiness.filter.full-filter')
                                        
                                        @include('template.bussiness.filter.full-category')

                                        @else
                                        @include('template.bussiness.filter.filter')
                                        
                                        @include('template.bussiness.filter.category')



@endif

                                    </div>
                                    <!--tabs end-->
                                </div>
                                <!-- listsearch-input-wrap end--> 

                            @endif                                   
                                <!-- listing-item-container -->
                                <div class="listing-item-container init-grid-items fl-wrap nocolumn-lic three-columns-grid">
                                   
                                     @if(isset($queries))
                                     @foreach ($queries as $query )
                               
                               <div class="listing-item">
                                   <article class="geodir-category-listing fl-wrap">
                                       <div class="geodir-category-img">
                                           <a href="{{route('bussiness.single',['bussiness'=>$query->slug])}}" class="geodir-category-img-wrap fl-wrap">
                                           @if($query->Hasmedia('images'))
                                                                   <img  src="{{$query->getFirstMediaUrl('images')}}" alt="title" title="title" >

                                                                   @elseif(findOriginCategoryJob($query->service)->Hasmedia('images'))
                                                                    
                                                                   <img  src="{{findOriginCategoryJob($query->service)->getFirstMediaUrl('images')}}" alt="title" title="title" >

                                                                    @else
                                                                    <img  src="{{asset('template/images/no-image.jpg')}}" alt="title" title="title" >

                                                             

                                                               @endif
                                           </a>
                                         
                                           <div class="geodir-category-opt">
                                               <div class="listing-rating-count-wrap">
                                                 
                                                   <br>
                                                   <div class="reviews-count">{{count($query->comments)}} {{__('cms.comment')}}</div>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="geodir-category-content fl-wrap title-sin_item">
                                           <div class="geodir-category-content-title fl-wrap">
                                               <div class="geodir-category-content-title-item">
                                                   <h3 class="title-sin_map"><a href="{{route('bussiness.single',['bussiness'=>$query->slug])}}">{{convert_lang($query,LaravelLocalization::getCurrentLocale(),'title')}} </a></h3>
                                                   <div class="geodir-category-location fl-wrap"><a href="https://maps.google.com/?q={{$query->address}}" ><i class="fas fa-map-marker-alt"></i> {{$query->address}} , {{$query->postal_code}} {{$query->city}}</a></div>
                                               </div>
                                           </div>
                                           <div class="geodir-category-text fl-wrap">
                                               <p class="small-text">  {{Str::limit(convert_lang($query,LaravelLocalization::getCurrentLocale(),'excerpt'), 25)}}</p>
                                               <div class="facilities-list fl-wrap">
                                                   <div class="facilities-list-title">{{__('cms.attributes')}} : </div>
                                                   <ul class="no-list-style">
                                                    @foreach ($attributes_job as $attr)
                                                    <li class="tolt"  data-microtip-position="top" data-tooltip=" {{convert_lang($attr,LaravelLocalization::getCurrentLocale(),'title')}}"><i class="{{$attr->icon}}" style="{{ in_array($attr->id,$query->attributes->toArray()) ? 'color:gray' : '' }}"></i></li>

                                                  @endforeach
                                                    

                                                    </ul>
                                               </div>
                                           </div>
                                           <div class="geodir-category-footer fl-wrap">
                                               <a class="listing-item-category-wrap">
                                                   <div class="listing-item-category" style="background-color:{{$query->list_category->color}}"><i class="{{$query->list_category->icon}}"></i></div>
                                                   <span> {{convert_lang($query->list_category,LaravelLocalization::getCurrentLocale(),'title')}}</span>
                                               </a>
                                               <div class="geodir-opt-list">
                                               <ul class="no-list-style">
                                                           <li><a href="#" class="show_gcc"><i class="fal fa-envelope"></i><span class="geodir-opt-tooltip"> {{__('cms.call')}}</span></a></li>
                                                           <li><a target='_blank' href="https://maps.google.com/?q={{$query->address}}" class="single-map-item" ><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip"> {{__('cms.location')}} </span> </a></li>

                                                           @if($query->Hasmedia(config("cms.collection-images")))
                                                           <li>
                                                           
                                                               <div class="dynamic-gal gdop-list-link" data-dynamicPath='[
                                                               @foreach($query->getMedia(config("cms.collection-images")) as $media)
                                                               {"src": "/media/{{$media->id}}/{{$media->file_name}}"},
                                                               @endforeach
                                                                ]'>
                         
                                                               <i class="fal fa-search-plus"></i><span class="geodir-opt-tooltip"> {{__('cms.gallery')}} </span></div>
                                                           </li>
                                                           @endif
                                                       </ul>
                                               </div>
                                              
                                               <div class="geodir-category_contacts">
                                                   <div class="close_gcc"><i class="fal fa-times-circle"></i></div>
                                                   <ul class="no-list-style">
                                                       <li><span><i class="fal fa-phone"></i> {{__('cms.call')}} : </span><a href="#">{{$query->phone}}</a></li>
                                                       <li><span><i class="fal fa-envelope"></i> {{__('cms.email')}} : </span><a href="#">{{$query->email}}</a></li>
                                                   </ul>
                                               </div>
                                           </div>
                                       </div>
                                   </article>
                               </div>
                            
                               @endforeach   

                                     @else

                                     @foreach ($all_bussiness as $bussiness )

<!-- listing-item  -->
<div class="listing-item">
    <article class="geodir-category-listing fl-wrap">
        <div class="geodir-category-img">
            <a href="{{route('bussiness.single',['bussiness'=>$bussiness->slug])}}" class="geodir-category-img-wrap fl-wrap">
            @if(!$bussiness->Hasmedia('images'))
                                    <img  src="{{$bussiness->getFirstMediaUrl('images')}}" alt="title" title="title" >
                                    @elseif(findOriginCategoryJob($bussiness->service)->Hasmedia('images'))
                                                                    
                                                                    <img  src="{{findOriginCategoryJob($bussiness->service)->getFirstMediaUrl('images')}}" alt="title" title="title" >
 
                                                                     @else
                                                                     <img  src="{{asset('template/images/no-image.jpg')}}" alt="title" title="title" >
 
                                                              

                               
                                @endif
            </a>
           
            <div class="geodir-category-opt">
                <div class="listing-rating-count-wrap">
                  
                  <br>
                    <div class="reviews-count">{{count($bussiness->comments)}} {{__('cms.comment')}}</div>
                </div>
            </div>
        </div>
        <div class="geodir-category-content fl-wrap title-sin_item">
            <div class="geodir-category-content-title fl-wrap">
                <div class="geodir-category-content-title-item">
                    <h3 class="title-sin_map"><a href="{{route('bussiness.single',['bussiness'=>$bussiness->slug])}}">{{convert_lang($bussiness,LaravelLocalization::getCurrentLocale(),'title')}} </a></h3>
                    <div class="geodir-category-location fl-wrap"><a href="https://maps.google.com/?q={{$bussiness->address}}" ><i class="fas fa-map-marker-alt"></i>  {{$bussiness->address}} , {{$bussiness->postal_code}} {{$bussiness->city}}</a></div>
                </div>
            </div>
                                          <div class="geodir-category-text fl-wrap">
                                               <p class="small-text"> {{$bussiness->title}}</p>
                                               <div class="facilities-list fl-wrap">
                                                   <div class="facilities-list-title">{{__('cms.attributes')}} : </div>
                                                   <ul class="no-list-style">
                                                      
                                                    @foreach ($attributes_job as $attr)
                                                    <li class="tolt"  data-microtip-position="top" data-tooltip=" {{convert_lang($attr,LaravelLocalization::getCurrentLocale(),'title')}}"><i class="{{$attr->icon}}" style="{{ in_array($attr->id,$bussiness->attributes->toArray()) ? 'color:gray' : '' }}"></i></li>

                                                  @endforeach
                                                    </ul>
                                               </div>
                                           </div>
            <div class="geodir-category-footer fl-wrap">
                <a class="listing-item-category-wrap">
                    <div class="listing-item-category"><i class="fal fa-cheeseburger"></i></div>
                    <span> {{convert_lang(showServiceCategory($bussiness->service),LaravelLocalization::getCurrentLocale(),'title')}}</span>
                </a>
                <div class="geodir-opt-list">
                <ul class="no-list-style">
                                                           <li><a href="#" class="show_gcc"><i class="fal fa-envelope"></i><span class="geodir-opt-tooltip"> {{__('cms.call')}}</span></a></li>
                                                           <li><a target='_blank' href="https://maps.google.com/?q={{$bussiness->address}}" class="single-map-item" ><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip"> {{__('cms.location')}} </span> </a></li>

                                                           @if($bussiness->Hasmedia(config("cms.collection-images")))
                                                           <li>
                                                           
                                                               <div class="dynamic-gal gdop-list-link" data-dynamicPath='[
                                                               @foreach($bussiness->getMedia(config("cms.collection-images")) as $media)
                                                               {"src": "/media/{{$media->id}}/{{$media->file_name}}"},
                                                               @endforeach
                                                                ]'>
                         
                                                               <i class="fal fa-search-plus"></i><span class="geodir-opt-tooltip"> {{__('cms.gallery')}} </span></div>
                                                           </li>
                                                           @endif
                                                       </ul>
                </div>
               
                <div class="geodir-category_contacts">
                    <div class="close_gcc"><i class="fal fa-times-circle"></i></div>
                    <ul class="no-list-style">
                        <li><span><i class="fal fa-phone"></i> {{__('cms.call')}} : </span><a href="#">{{$bussiness->phone}}</a></li>
                        <li><span><i class="fal fa-envelope"></i> {{__('cms.email')}} : </span><a href="#">{{$bussiness->email}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </article>
</div>
<!-- listing-item end --> 


@endforeach     

                                     @endif
                                    
                                    

                            

                                                                                       
                                    <!-- <div class="pagination fwmpag">
                                        <a href="#" class="prevposts-link"><i class="fas fa-caret-right"></i><span>قبلی</span></a>
                                        <a href="#">1</a>
                                        <a href="#" class="current-page">2</a>
                                        <a href="#">3</a>
                                        <a href="#">...</a>
                                        <a href="#">7</a>
                                        <a href="#" class="nextposts-link"><span>بعدی</span><i class="fas fa-caret-left"></i></a>
                                    </div> -->
                                </div>
                                <!-- listing-item-container end -->
                            </div>
                        </div>
                    </section>
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection
@section('scripts')
@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<script src="{{asset('template/js/shop.js')}}" defer></script>
<link type="text/css" rel="stylesheet" href="{{asset('template/css/shop.css?')}}{{uniqid()}}">

@else

<script src="{{asset('template/js/ltr-shop.js')}}" defer></script>
<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-shop.css?')}}{{uniqid()}}">


@endif


<script>



function loadSubCategory(e) {


     
$category= e.target.value;
$lang=$("html").attr("lang");




$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/sub/category/bussiness/'+$category+"/"+$lang,
    data: { field1:$category,field2:$lang} ,
    beforeSend:function(){
    
},
beforeSend:function(){
    $('#sub_category + .nice-select').attr('style','opacity:0');
},

success: function (response) {


   if(response.data.status){

       console.log(response.data);

       $('#sub_category').empty();
     
       $('#sub_category + .nice-select .list').empty();

       $('#sub_category + .nice-select > span.current').text('');

       $('#sub_category + .nice-select .list').empty();
       
     
     //start for each for load select input 

       $.each(response.data.result, function(key, index) {
   
                        if(key==0){
                                $('#sub_category').append("<option value='"+index.id+"'   selected>"+index.translate+"</option>");
                                $('#sub_category + .nice-select > span.current').text(index.translate);
                            }
                            else{
                                $('#sub_category').append("<option value='"+index.id+"'   >"+index.translate+"</option>");
                                $('#sub_category + .nice-select  ul.list ').append("<li class='option'  data-value='"+index.id+"'   >"+index.translate+"</li>"); 
                            }
         
 
        });

       }


  //end for each for load select input 

   
},
error: function (xhr,ajaxOptions,thrownError) {

console.log(xhr,ajaxOptions,thrownError);

    $("#sub_category").empty();
    $('#sub_category + .nice-select .list').empty();
    $('#sub_category + .nice-select > span.current').text('');
    $('#sub_category + .nice-select').attr('style','opacity:0');

  
},
complete:function(){
    $('#sub_category + .nice-select').attr('style','opacity:1');
}

});
}




function loadCity(e) {

var myInput = document.getElementById("postal_code");
     


if(myInput.value.length==5){



console.log($('#latitude').val());
          

    $code=$("#postal_code").val();
   

$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/city/postalcode/'+$code,
    data: { field1:$code} ,
    beforeSend:function(){
        $('#city + .nice-select').attr('style','opacity:0');
},
    success: function (response) {
       if(response.data.status){

           console.log(response.data);
           $('#city').empty();
           $('#city + .nice-select .list').empty();
           $('#city + .nice-select > span.current').text('');
         
            $('#city').append("<option value='"+response.data.result[0].fields.plz_name+"' selected>"+response.data.result[0].fields.plz_name+"</option>");
           
           
            $('#city + .nice-select .list').empty();
                $('#city + .nice-select .list').append("<li data-value='"+response.data.result[0].fields.plz_name+"' class='option  selected focus'>"+response.data.result[0].fields.plz_name+"</li>");
           
                $('#city + .nice-select > span.current').text(response.data.result[0].fields.plz_name);

              

       }
       else{
        console.log('no');
       }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
    $('#city + .nice-select > span.current').text('');
    $('#city + .nice-select').attr('style','opacity:0');

    },
    complete:function(){
        $('#city + .nice-select').attr('style','opacity:1');
}

});

}
}

</script>


@endsection






