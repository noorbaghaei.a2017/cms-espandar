<!--multi section start-->

<section>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12 col-md-12">
                <div class="owl-carousel owl-theme no-pb xs-text-center" data-dots="false" data-items="3" data-margin="30" data-autoplay="true">
                    @foreach($agents as $agent)
                    <div class="item">
                        @if(!$agent->Hasmedia('images'))
                            <img src="{{asset('img/no-img.gif')}}" alt="{{$agent->name}}" title="{{$agent->name}}" class="img-center" width="60">
                        @else
                            <img src="{{$agent->getFirstMediaUrl('images')}}" alt="{{$agent->name}}" title="{{$agent->name}}" class="img-center">
                        @endif
                    </div>
                    @endforeach
                </div>
                <div class="row mt-5">
                    <div class="col-sm-12">
                        <div class="tab">
                            <!-- Nav tabs -->
                            <nav class="tab-agents">
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    @foreach($agents as $agent)
                                        @if($loop->first)
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#{{$agent->token}}" role="tab" aria-selected="true">{!! convert_lang($agent,LaravelLocalization::getCurrentLocale(),'name') !!} </a>
                                        @else
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#{{$agent->token}}" role="tab" aria-selected="false">  {!! convert_lang($agent,LaravelLocalization::getCurrentLocale(),'name') !!}</a>
                                        @endif
                                    @endforeach
                                </div>
                            </nav>
                            <!-- Tab panes -->
                            <div class="tab-content pl-3 pt-3 pb-0" id="nav-tabContent">
                                @foreach($agents as $agent)
                                    @if($loop->first)
                                <div role="tabpanel" class="tab-pane fade show active" id="{{$agent->token}}">
                                    <div class="row text-black mt-4">
                                        <div class="col-sm-6">
                                            {!! convert_lang($agent,LaravelLocalization::getCurrentLocale(),'text') !!}
                                        </div>

                                    </div>
                                </div>
                                    @else
                                <div role="tabpanel" class="tab-pane fade" id="{{$agent->token}}">

                                    <div class="row text-black mt-4">
                                        <div class="col-sm-6">
                                            {!! convert_lang($agent,LaravelLocalization::getCurrentLocale(),'text') !!}
                                        </div>

                                    </div>
                                </div>
                                    @endif
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<!--multi section end-->
