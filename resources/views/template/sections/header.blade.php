
<!DOCTYPE HTML>
<html lang="{{LaravelLocalization::getCurrentLocale()}}">
    <head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        {!! SEO::generate() !!}

            @yield('seo')
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">


        <meta name="google-site-verification" content="XM1lZFrEWSpl9ybBUmhC8byffubviu84b0pOa9C5zMc" />


        <meta name="csrf-token" content="{{ csrf_token() }}">
       
        <!--=============== css  ===============-->	
      
        <link type="text/css" rel="stylesheet" href="{{asset('template/css/reset.css')}}" >

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

            <link type="text/css" rel="stylesheet" href="{{asset('template/css/style.css?')}}{{uniqid()}}">
            
            @else

            <link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-style.css?')}}{{uniqid()}}">

          
            @endif


        <!-- Date Picker CSS -->
        <!-- <link rel="stylesheet" href="{{asset('template/css/date-picker.css')}}"> -->
        <!-- persian picker CSS -->
        <!-- <link rel="stylesheet" href="src/jquery.md.bootstrap.datetimepicker.style.css" /> -->

        <!--=============== favicons ===============-->
          <!-- Favicon -->
    @if(!$setting->Hasmedia('logo'))
        <link  rel="preload shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon preload" href="{{asset('img/no-img.gif')}}">
    @else
        <link rel="shortcut icon preload" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon preload" href="{{$setting->getFirstMediaUrl('logo')}}">
    @endif
    <!-- Stylesheets -->

    @yield('heads')

    </head>

    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="loader-inner">
                <div class="loader-inner-cirle"></div>
            </div>
        </div>
        <!--loader end-->
        <!-- main start  -->
        <div id="main">
            <!-- header -->
            <header class="main-header">
                <!-- logo-->
                <a href="{{route('front.website')}}" class="logo-holder">
                @if(!$setting->Hasmedia('logo'))
                    <img class="lazy-logo" data-logo="{{asset('img/no-img.gif')}}" alt="">
                    @else
                    <img class="lazy-logo" data-logo="{{$setting->getFirstMediaUrl('logo')}}" alt="">

                    @endif
                </a>
                <!-- logo end-->
                 <!-- header-search_btn-->         
                 <div class="header-search_btn show-search-button"><i class="fal fa-search"></i></div>
                <!-- header-search_btn end-->


                <!-- header opt --> 
                @if(auth('client')->check())
                <a title="{{__('cms.add_bussiness')}}" href="{{route('client.create.bussiness')}}" class="add-list color-bg"><span><i class="fal fa-layer-plus"></i></span></a>
                @if(getFavoritesClient(auth('client')->user()->id)->count() > 0)
                <div class="cart-btn   show-header-modal" data-microtip-position="bottom" role="tooltip" aria-label="Your Wishlist"><i class="fal fa-heart"></i><span class="cart-counter green-bg"></span> </div>
               @endif
               <div class="header-user-menu">
                    <div class="header-user-name">
                        <span>
                        
                        @if(!$client->Hasmedia('images'))
                        <img src="{{asset('template/images/user-icon-2.jpg')}}" alt="">
                        @else

                        <img src="{{$client->getFirstMediaUrl('images')}}" alt="">
                       
                        @endif
                        </span>
                       
                    </div>
                    <ul>
                        <li><a href="{{route('client.dashboard')}}"> {{__('cms.dashboard')}}  </a></li>
                        <li><a href="{{route('client.edit')}}"> {{__('cms.edit')}}  </a></li>
                        <li><a href="{{route('client.create.bussiness')}}"> {{__('cms.jobs')}}  </a></li>
                        <li><a href="{{route('client.show.cart')}}"> {{__('cms.order')}}  </a></li>
                        <li><a href="{{route('client.logout.panel')}}">{{__('cms.logout')}} </a></li>
                   
                    </ul>
                </div>
               @else
               <div class="show-reg-form modal-open avatar-img" data-srcav="{{asset('template/images/avatar/3.jpg')}}"><i class="fal fa-user"></i>{{__('cms.login')}}</div>


               @endif
               
                <!-- header opt end--> 

                

                <!-- lang-wrap-->
                <div class="lang-wrap">
               
                    <div class="show-lang"><span> <i class="fal fa-globe-europe"></i><strong>{{LaravelLocalization::getCurrentLocale()}}</strong> <i class="fa fa-caret-down arrlan"></i></span></div>
                    <ul class="lang-tooltip  no-list-style">
                                 @foreach(config('cms.all-lang') as $language)
                                    @if(LaravelLocalization::getCurrentLocale()===$language)
                                    <li><a href="#" class="current-lan" data-lantext="{{$language}}">{{__('cms.lang.'.$language)}}</a></li>
                        
                                      @else
                                      <li><a href="{{setLangFront(LaravelLocalization::getCurrentLocale(),$language)}}" data-lantext="Fr">{{__('cms.lang.'.$language)}}</a></li>
     
                                     @endif
                                @endforeach
                      
                       
                    </ul>
                   
                </div>
                <!-- lang-wrap end-->  





                

                <!-- nav-button-wrap--> 
                <div class="nav-button-wrap color-bg">
                    <div class="nav-button">
                        <span></span><span></span><span></span>
                    </div>
                </div>
                <!-- nav-button-wrap end-->


                <!--  navigation --> 
                <div class="nav-holder main-menu" >
                    <nav>
                        <ul class="no-list-style" >
                       
                        @foreach ($top_menus as  $menu)

                       

                        @if($menu->pattern=="category")
  
          
                                    <li ><a href="{{$menu->href}}">{!! convert_lang($menu,LaravelLocalization::getCurrentLocale(),'symbol') !!} <i class="fa fa-caret-down"></i></a>

                                        <!--third  level  -->
                                        <ul style="min-width: 200px;">
                                        @foreach ($all_category_bussiness_top as $category)

                                        @if ($category->childs())

                                            <li  ><a href="{{route('search.page.category.bussiness',['filter'=>0,'category'=>$category->id])}}">{!! convert_lang($category,LaravelLocalization::getCurrentLocale(),'title') !!}<i class="fa fa-caret-down"></i></a>

                                                                <!--third  level  -->
                                                <ul >
                                                    @foreach ($category->childs()->get()  as $child)
                                                    <? return dd($child_active_category); ?>
                                                    {{-- @if(in_array($child->id,$child_active_category)) --}}
                                                    @if(in_array($child->id,$child_active_category))
                                                    <li ><a href="{{route('search.page.bussiness',['filter'=>0,'category'=>$child->id])}}">{!! convert_lang($child,LaravelLocalization::getCurrentLocale(),'title') !!}</a></li>
  
                                                    @endif
                                                    @endforeach
                                                     <li class="btn_more_menu"><a  href="{{route('search.show.category.bussiness',['category'=>$category->id])}}" >{{__('cms.more')}}</a></li>

                                                </ul>
                                                <!--third  level end-->
                                           
                                        
                                       
                                        @else

                                      

                                            <li class="child-category-mobile" style="width:32%;"><a href="{{route('search.page.bussiness',['filter'=>0,'category'=>$category->id])}}">{!! convert_lang($category,LaravelLocalization::getCurrentLocale(),'title') !!}</a>

                                      
                                        @endif
                                        </li>
                                            @endforeach
                                            <li class="btn_more_menu"><a  href="{{route('search.show.category.bussiness',['category'=>0])}}" >{{__('cms.more')}}</a></li>

                                       
                                        </ul>
                                        <!--third  level end-->

                                      

                                    </li>
                                   
                              

                      
                       

                        @elseif($menu->pattern=="search")
                        <li><a href="{{route('search.page.bussiness',['filter'=>1])}}">{!! convert_lang($menu,LaravelLocalization::getCurrentLocale(),'symbol') !!}

                        @else
                       
                        <li><a href="{{$menu->href}}">{!! convert_lang($menu,LaravelLocalization::getCurrentLocale(),'symbol') !!}
                            @if(count($menu->childs)>0)
                            <i class="fa fa-caret-down"></i>
                            @endif
                            </a>
                        
                        
                        @if(count($menu->childs) > 0)
                       
                       
                                <!--third  level  -->
                                <ul>
                                @foreach ($menu->childs as  $child_menu)

                                    <li><a href="{{$child_menu->href}}">{!! convert_lang($child_menu,LaravelLocalization::getCurrentLocale(),'symbol') !!}</a></li>
                                @endforeach

                                </ul>
                                <!--third  level end-->
                       

                        @endif
                        </li>

                        @endif

                        @endforeach
                   
                        </ul>
                    </nav>
                </div>
                <!-- navigation  end -->


 <!-- header-search_container -->                     
 <div class="header-search_container header-search vis-search">
                    <div class="container small-container">
                        <form action="{{route('search.page.bussiness',['filter'=>1])}}" method="GET" class="header-search-input-wrap fl-wrap">
                         
                        <!-- header-search-input --> 
                            <div class="header-search-input">
                                <label><i class="fal fa-keyboard"></i></label>
                                <input type="text" placeholder="{{__('cms.what')}}"   value=""/>  
                            </div>
                            <!-- header-search-input end -->  
                            <!-- header-search-input --> 
                            <div class="header-search-input location autocomplete-container">
                                <label><i class="fal fa-map-marker"></i></label>
                                <input name="wo" type="text" placeholder="{{__('cms.where')}}" class="autocomplete-input" id="autocompleteid2" value=""/>
                                <a href="#"><i class="fal fa-dot-circle"></i></a>
                            </div>
                            <!-- header-search-input end -->                                        
                            <!-- header-search-input --> 
                            <div class="header-search-input header-search_selectinpt ">
                                <select name="was" data-placeholder="Category" class="chosen-select no-radius" >
                                    
                                @foreach($all_category_bussiness_top as $parent)
                                    <option value="{{$parent->title}}">{{convert_lang($parent,LaravelLocalization::getCurrentLocale(),'title')}}</option>
                                  
                                    @endforeach
                                </select>
                            </div>
                            <!-- header-search-input end --> 
                            <button type="submit" class="header-search-button green-bg" ><i class="far fa-search"></i> {{__('cms.search')}} </button>
</form>
                        <div class="header-search_close color-bg"><i class="fal fa-long-arrow-up"></i></div>
                    </div>
                </div>
                <!-- header-search_container  end --> 

                @if(auth('client')->check())

   <!-- wishlist-wrap--> 
   <div class="header-modal novis_wishlist">
                    <!-- header-modal-container--> 
                    <div class="header-modal-container scrollbar-inner fl-wrap" data-simplebar>
                        <!--widget-posts-->
                        <div class="widget-posts  fl-wrap">
                            <ul class="no-list-style">
                               
                            @foreach(getFavoritesClient(auth('client')->user()->id) as $favorite)
                                <li>
                                    <div class="widget-posts-img"><a href="{{route('bussiness.single',['bussiness'=>$favorite->slug])}}">
                                    @if(!$favorite->Hasmedia('images'))
                                                                   <img  src="{{asset('img/no-img.gif')}}" alt="title" title="title" >
                                                               @else
                                                               <img  src="{{$favorite->getFirstMediaUrl('images')}}" alt="title" title="title" >
                                                               @endif   
                                  
                                    </a>
                                    </div>
                                    <div class="widget-posts-descr">
                                        <h4><a href="{{route('bussiness.single',['bussiness'=>$favorite->slug])}}">{{convert_lang($favorite,LaravelLocalization::getCurrentLocale(),'title')}}  </a></h4>
                                        <div class="geodir-category-location fl-wrap"><a target="_blank" href="https://maps.google.com/?q={{$favorite->address}}"><i class="fas fa-map-marker-alt"></i> {{$favorite->address}} , {{$favorite->postal_code}} {{$favorite->city}} </a></div>
                                        
                                       
                                    </div>
                                </li>
                                @endforeach
                               
                               
                            </ul>
                        </div>
                        <!-- widget-posts end-->
                    </div>
                    <!-- header-modal-container end--> 
                    <div class="header-modal-top fl-wrap">
                    <a style="color:#fdfdfd"  href="{{route('client.favorites.job')}}">{{__('cms.more')}}</a>
                        <div class="close-header-modal"><i class="far fa-times"></i></div>
                        
                    </div>
                </div>
                <!--wishlist-wrap end --> 

@endif

               
                
            </header>
            <!-- header end-->
            <!-- wrapper-->
            <div id="wrapper">





