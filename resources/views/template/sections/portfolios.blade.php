<!--project start-->

<section class="o-hidden">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-8 col-md-10 ml-auto mr-auto">
                <div class="section-title">
                    <h2 class="title">نمونه کارها</h2>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-12">
                <div class="portfolio-filter">
                    <button data-filter="" class="is-checked">همه</button>
                    <button data-filter=".cat1">مکانیکی</button>
                    <button data-filter=".cat2">لوله کشی</button>
                    <button data-filter=".cat3">جوشکاری</button>
                    <button data-filter=".cat4">شیمیایی</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="masonry row columns-4 no-gutters popup-gallery">
                    <div class="grid-sizer"></div>
                    <div class="masonry-brick cat3">
                        <div class="portfolio-item">
                            <img src="{{asset('template/images/portfolio/masonry/01.jpg')}}" alt="">
                            <div class="portfolio-hover">
                                <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                    <h4>عنوان پروژه</h4>
                                </div>
                                <div class="portfolio-icon">
                                    <a class="popup popup-img" href="{{asset('template/images/portfolio/large/01.jpg')}}"> <i class="flaticon-magnifier"></i>
                                    </a>
                                    <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="masonry-brick cat3">
                        <div class="portfolio-item">
                            <img src="{{asset('template/images/portfolio/masonry/03.jpg')}}" alt="">
                            <div class="portfolio-hover">
                                <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                    <h4>عنوان پروژه</h4>
                                </div>
                                <div class="portfolio-icon">
                                    <a class="popup popup-img" href="{{asset('template/images/portfolio/large/03.jpg')}}"> <i class="flaticon-magnifier"></i>
                                    </a>
                                    <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="masonry-brick cat4">
                        <div class="portfolio-item">
                            <img src="images/portfolio/masonry/02.jpg" alt="">
                            <div class="portfolio-hover">
                                <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                    <h4>عنوان پروژه</h4>
                                </div>
                                <div class="portfolio-icon">
                                    <a class="popup popup-img" href="{{asset('template/images/portfolio/large/02.jpg')}}"> <i class="flaticon-magnifier"></i>
                                    </a>
                                    <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="masonry-brick cat2">
                        <div class="portfolio-item">
                            <img src="{{asset('template/images/portfolio/masonry/04.jpg')}}" alt="">
                            <div class="portfolio-hover">
                                <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                    <h4>عنوان پروژه</h4>
                                </div>
                                <div class="portfolio-icon">
                                    <a class="popup popup-img" href="{{asset('template/images/portfolio/large/04.jpg')}}"> <i class="flaticon-magnifier"></i>
                                    </a>
                                    <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="masonry-brick cat1">
                        <div class="portfolio-item">
                            <img src="{{asset('template/images/portfolio/masonry/07.jpg')}}" alt="">
                            <div class="portfolio-hover">
                                <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                    <h4>عنوان پروژه</h4>
                                </div>
                                <div class="portfolio-icon">
                                    <a class="popup popup-img" href="{{asset('template/images/portfolio/large/07.jpg')}}"> <i class="flaticon-magnifier"></i>
                                    </a>
                                    <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="masonry-brick cat1">
                        <div class="portfolio-item">
                            <img src="{{asset('template/images/portfolio/masonry/08.jpg')}}" alt="">
                            <div class="portfolio-hover">
                                <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                    <h4>عنوان پروژه</h4>
                                </div>
                                <div class="portfolio-icon">
                                    <a class="popup popup-img" href="{{asset('template/images/portfolio/large/08.jpg')}}"> <i class="flaticon-magnifier"></i>
                                    </a>
                                    <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="masonry-brick cat3">
                        <div class="portfolio-item">
                            <img src="{{asset('template/images/portfolio/masonry/06.jpg')}}" alt="">
                            <div class="portfolio-hover">
                                <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                    <h4>عنوان پروژه</h4>
                                </div>
                                <div class="portfolio-icon">
                                    <a class="popup popup-img" href="{{asset('template/images/portfolio/large/06.jpg')}}"> <i class="flaticon-magnifier"></i>
                                    </a>
                                    <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="masonry-brick cat4">
                        <div class="portfolio-item">
                            <img src="{{asset('template/images/portfolio/masonry/05.jpg')}}" alt="">
                            <div class="portfolio-hover">
                                <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                    <h4>عنوان پروژه</h4>
                                </div>
                                <div class="portfolio-icon">
                                    <a class="popup popup-img" href="{{asset('template/images/portfolio/large/05.jpg')}}"> <i class="flaticon-magnifier"></i>
                                    </a>
                                    <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="masonry-brick cat4">
                        <div class="portfolio-item">
                            <img src="{{asset('template/images/portfolio/masonry/09.jpg')}}" alt="">
                            <div class="portfolio-hover">
                                <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                    <h4>عنوان پروژه</h4>
                                </div>
                                <div class="portfolio-icon">
                                    <a class="popup popup-img" href="{{asset('template/images/portfolio/large/09.jpg')}}"> <i class="flaticon-magnifier"></i>
                                    </a>
                                    <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="masonry-brick cat1">
                        <div class="portfolio-item">
                            <img src="{{asset('template/images/portfolio/masonry/10.jpg')}}" alt="">
                            <div class="portfolio-hover">
                                <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                    <h4>عنوان پروژه</h4>
                                </div>
                                <div class="portfolio-icon">
                                    <a class="popup popup-img" href="{{asset('template/images/portfolio/large/10.jpg')}}"> <i class="flaticon-magnifier"></i>
                                    </a>
                                    <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--project end-->
