<!-- Brand Area Start Here -->
<section class="brand-wrap-layout1 bg--light padding-top-9p6 padding-bottom-7">
    <div class="container">
        <div class="rc-carousel" data-loop="true" data-items="6" data-margin="30" data-autoplay="true"
             data-autoplay-timeout="5000" data-smart-speed="2000" data-dots="false" data-nav="false"
             data-nav-speed="false" data-r-x-small="2" data-r-x-small-nav="false" data-r-x-small-dots="false"
             data-r-x-medium="2" data-r-x-medium-nav="false" data-r-x-medium-dots="false" data-r-small="3"
             data-r-small-nav="false" data-r-small-dots="false" data-r-medium="4" data-r-medium-nav="false"
             data-r-medium-dots="false" data-r-large="5" data-r-large-nav="false" data-r-large-dots="false"
             data-r-extra-large="5" data-r-extra-large-nav="false" data-r-extra-large-dots="false">
            @foreach($brands as $brand)
            <div class="brand-box-layout1">
                @if(!$brand->Hasmedia('images'))
                    <img src="{{asset('img/no-img.gif')}}" alt="Brand" class="img-fluid">
                @else
                    <img src="{{$brand->getFirstMediaUrl('images')}}" alt="Brand" class="img-fluid">
                @endif
            </div>
            @endforeach

        </div>
    </div>
</section>
<!-- Brand Area End Here -->
