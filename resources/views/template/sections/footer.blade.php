</div>
            <!-- wrapper end-->

            <!--footer -->
            <footer class="main-footer fl-wrap">
               
                <!--footer-inner-->
                <div class="footer-inner   fl-wrap">
                    <div class="container">
                        <div class="row">

                               
                                 <!-- footer-widget-->
                            <div class="col-md-4">
                                <div class="footer-widget fl-wrap">
                                    <div class="footer-logo"><a href="{{route('front.website')}}">
                                    @if(!$setting->Hasmedia('logo'))
                    <img class="lazy-logo" data-logo="{{asset('img/no-img.gif')}}" alt="">
                    @else
                    <img class="lazy-logo" data-logo="{{$setting->getFirstMediaUrl('logo')}}" alt="">

                    @endif
                                       </a>
                                    </div>
                                    <div class="footer-contacts-widget fl-wrap">
                                        <p> {!! convert_lang($setting,LaravelLocalization::getCurrentLocale(),'slogan') !!} </p>
                                        <ul  class="footer-contacts fl-wrap no-list-style">
                                            <li><span><i class="fal fa-envelope"></i> {{__('cms.email')}} :</span><a href="mailto::{{$setting->email}}" target="_blank">{{$setting->email}}</a></li>
                                            <li> <span><i class="fal fa-map-marker"></i> {{__('cms.address')}} :</span><a target='_blank' href="https://maps.google.com/?q={{$setting->address}}">{{$setting->address}} </a></li>
                                            <li><span><i class="fal fa-phone"></i> {{__('cms.phone')}} :</span><a href="tel::{{$setting->phone}}">{{$setting->phone}}</a></li>
                                        </ul>
                                        <div class="footer-social">
                                            <span>{{__('cms.follow_us')}}: </span>
                                            <ul class="no-list-style">
                                                
                                                @if(!is_null($setting->info->telegram))
                                                <li><a href="https://telegram.me/{{$setting->info->telegram}}" target="_blank"><i class="fab fa-telegram"></i></a></li>
                                                @endif
                                              
                                                @if(!is_null($setting->info->instagram))
                                                <li><a href="https://www.instagram.com/{{$setting->info->instagram}}" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                                @endif
                                                @if(!is_null($setting->info->whatsapp))
                                                <li><a href="https://wa.me/{{$setting->info->whatsapp}}" target="_blank"><i class="fab fa-whatsapp"></i></a></li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- footer-widget end-->
                            @if($last_articles->count() > 0)
                            <!-- footer-widget-->
                            <div class="col-md-4">
                                <div class="footer-widget fl-wrap">
                                    <h3>{{__('cms.last_article')}}  </h3>
                                    <div class="footer-widget-posts fl-wrap">
                                        <ul class="no-list-style">
                                        @foreach ($last_articles as $article)
                                        <li class="clearfix">
                                                <a href="#"  class="widget-posts-img"><img src="{{asset('template/images/all/4.jpg')}}" class="respimg" alt=""></a>
                                                <div class="widget-posts-descr">
                                                    <a href="#" title="">{{$article->title}} </a>
                                                    <span class="widget-posts-date"><i class="fal fa-calendar"></i> 2 min ago </span> 
                                                </div>
                                            </li>
                                        @endforeach
                                           
                                           
                                           
                                        </ul>
                                        @if(count($last_articles) > 0)
                                        <a href="{{route('articles')}}" class="footer-link">{{__('cms.all')}}  <i class="fal fa-long-arrow-right"></i></a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- footer-widget end-->
                            @endif
                            <!-- footer-widget  -->
                            <div class="col-md-4">
                                <div class="footer-widget fl-wrap ">
                                   
                                    <div class="twitter-holder fl-wrap scrollbar-inner2" data-simplebar data-simplebar-auto-hide="false">
                                        <div id="footer-twiit"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- footer-widget end-->
 
                        </div>
                    </div>
                    <!-- footer bg-->
                    <div class="footer-bg" data-ran="4"></div>
                    <div class="footer-wave">
                        <svg viewbox="0 0 100 25">
                            <path fill="#fff" d="M0 30 V12 Q30 6 55 12 T100 11 V30z" />
                        </svg>
                    </div>
                    <!-- footer bg  end-->
                </div>
                <!--footer-inner end -->


                  <!--sub-footer-->
                  <div class="sub-footer  fl-wrap">
                    <div class="container">
                        <div class="copyright"> &#169; {!! convert_lang($setting,LaravelLocalization::getCurrentLocale(),'copy_right') !!}</div>
                       
                        <div class="subfooter-nav">
                            <ul class="no-list-style">
                            <li><a href="{{route('privacy')}}">{{__('cms.privacy')}}</a></li>
                                <li><a href="{{route('imprint')}}">{{__('cms.imprint')}}   </a></li>
                           
                            </ul>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                </div>
                <!--sub-footer end -->

            </footer>
            <!--footer end -->  
            <!--map-modal -->
            <div class="map-modal-wrap">
                <div class="map-modal-wrap-overlay"></div>
                <div class="map-modal-item">
                    <div class="map-modal-container fl-wrap">
                        <div class="map-modal fl-wrap">
                            <div id="singleMap" data-latitude="40.7" data-longitude="-73.1"></div>
                        </div>
                        <h3><span>مکان برای : </span><a href="#">عنوان لیست</a></h3>
                        <div class="map-modal-close"><i class="fal fa-times"></i></div>
                    </div>
                </div>
            </div>
            <!--map-modal end -->                
            <!--register form -->
            <div class="main-register-wrap modal">
                <div class="reg-overlay"></div>
                <div class="main-register-holder tabs-act">
                    <div class="main-register fl-wrap  modal_main">
                        <div class="main-register_title"> <span>  iniaz</span></div>
                        <div class="close-reg"><i class="fal fa-times"></i></div>
                        <div>
                        <p id="result-ajax"> </p>

                            </div>
                        <ul id="tab-menu-login" class="tabs-menu fl-wrap no-list-style">
                            <li class="current"><a href="#tab-1"><i class="fal fa-sign-in-alt"></i> {{__('cms.login')}}</a></li>
                            <li id="register-tab"><a href="#tab-2"><i class="fal fa-user-plus"></i>  {{__('cms.register')}}</a></li>
                            <li><a href="#tab-3"><i class="fal fa-key"></i>  {{__('cms.forget_password')}}</a></li>

                        </ul>
                        <!--tabs -->                       
                        <div class="tabs-container">
                            <div class="tab">
                                <!--tab -->
                                <div id="tab-1" class="tab-content first-tab">
                                    <div class="custom-form">
                                    <form id="form-login" action="{{route('login.ajax')}}" method="POST">
                                            
                                            <label>{{__('cms.username')}}   <span>*</span> </label>
                                            <input name="email" type="text" name="emai" id="email"   onClick="this.select()" value="">
                                            <label > {{__('cms.password')}} <span>*</span> </label>
                                            <input name="password" name="password" type="password" id="password"    onClick="this.select()" value="" >
                                            <button id="btn-login"  type="submit"  class="btn float-btn color2-bg"> {{__('cms.login')}} <i class="fas fa-caret-right"></i></button>
                                            <div class="clearfix"></div>
                                            
                                    
                                      </form>
                                    </div>
                                </div>
                                <!--tab end -->
                                <!--tab -->
                                <div class="tab">
                                    <div id="tab-2" class="tab-content">
                                        <div class="custom-form">
                                            <form id="form-register" action="{{route('register.ajax')}}" method="POST">
                                            
                                            <label > {{__('cms.first_name')}}  <span>*</span> </label>
                                            <input  id="first_name_r" type="text" name="first"   onClick="this.select()" value="">
                                                <label > {{__('cms.last_name')}}  <span>*</span> </label>
                                                <input  id="last_name_r" type="text" name="last"   onClick="this.select()" value="">
                                                <label>{{__('cms.email')}}  <span>*</span></label>
                                                <input  id="email_r" type="text" name="email"  onClick="this.select()" value="">
                                                <label > {{__('cms.password')}} <span>*</span></label>
                                                <input  id="password_r" type="password" name="password"  onClick="this.select()" value="" >
                                                <div class="filter-tags ft-list">
                                                    <input id="law" type="checkbox" name="law">
                                                    <label for="law"> {{__('cms.agree')}} <a href="{{route('privacy')}}" target="_blank">{{__('cms.agree_law')}}</a></label>
                                                </div>
                                                <br>
                                                <div class="filter-tags ft-list">
                                                    <input id="nut" type="checkbox" name="impressum">
                                                    <label for="law"> {{__('cms.agree')}} <a href="{{route('imprint')}}" target="_blank">{{__('cms.agree_nut')}}</a></label>
                                                </div>
                                                <br>
                                                <div class="filter-tags ft-list">
                                                    <input id="notification_email" type="checkbox" name="notification_email">
                                                    <label for="notification_email">  {{__('cms.notification_email')}}</label>
                                                </div>
                                                <br>
                                              
                                                <div class="clearfix"></div>
                                                
                                                <button id="btn-register"  type="submit"    class="btn float-btn color2-bg">  {{__('cms.register')}}  <i class="fas fa-caret-right"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!--tab end -->

                                <!--tab -->
                                <div class="tab">
                                    <div id="tab-3" class="tab-content">
                                    <span id="code_hidden" data-client="" style="display:none"></span>
                                        <form id="form-sendcode" action="{{route('verify.email.ajax')}}" class="custom-form" method="POST" >
                                            <div class="box-email">
                                            <label > {{__('cms.email')}}  <span>*</span> </label>
                                            <input  id="email_f" type="text" name="email"   onClick="this.select()" value="">
                                            <input  id="code_f" type="text" name="code"  value="" hidden>

                                            </div>
                                                <div class="clearfix"></div>
                                                <button id="btn-sendCode"       class="btn float-btn color2-bg">  {{__('cms.send-code')}}  <i class="fas fa-caret-right"></i></button>
                                           
                                        </form>
                                    </div>
                                </div>
                                <!--tab end -->


                            </div>
                            <!--tabs end -->
                           
                            <div class="wave-bg">
                                <div class='wave -one'></div>
                                <div class='wave -two'></div>
                            </div>
                         
                        </div>
                    </div>
                </div>
            </div>
            <!--register form end -->
            <a class="to-top"><i class="fas fa-caret-up"></i></a>  
            
            
@if(!auth('client')->check() &&  $noExistClient)

<div id="cookieConsent">
    <div id="closeCookieConsent">x</div>
    <div style="text-align:left;">
  
    <input id="ip_item" type="checkbox" name="ip" checked value="">
    <label>Erlauben Ip</label>
    <br>
   
    <input id="analytic_item" type="checkbox" name="analyitics" checked value="">
    <label>Erlauben Analytics</label>
    </br>
    </br>
</div>
     <a class="cookieConsentOK" class="btn btn-primary" style="float:left">Abbrechen</a> <a id="result_check" class="cookieConsentOK">Akzeptieren</a>
</div>


@endif
            
        </div>

        <!-- Main end -->
        
        @if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")


            <link type="text/css" rel="stylesheet" href="{{asset('template/css/rtl-plugins.css?')}}{{uniqid()}}">
            
            @else

            <link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-plugins.css?')}}{{uniqid()}}">

          
            @endif

        
       
        <link type="text/css" rel="preload stylesheet" href="{{asset('template/css/color.css')}}">

        <!--=============== scripts  ===============-->
        <script src="{{asset('template/js/jquery.min.js')}}"></script>
      
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> --}}
        <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDo7Jn-1Lc1tBg1-GrOMJbZzYVTfi2lYNc&libraries=places&callback=initAutocomplete"></script> -->
        <script src="{{asset('template/js/plugins.js')}}" ></script>
        @if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

       
        <script src="{{asset('template/js/rtl-scripts.js')}}" ></script>
      
        @else
      
        <script src="{{asset('template/js/ltr-scripts.js')}}" ></script>
    

        @endif
      
        {{-- <script type="text/javascript" src="{{asset('template/js/jquery.lazy.min.js')}}"></script> --}}


        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.11/jquery.lazy.min.js"></script>
        @yield('scripts')
  

        <script>


$(function() {
        $('.lazy').lazy({
            attribute: "data-fix",
           
        });
       
        $('.lazy-logo').lazy({
            attribute: "data-logo",
            
        });

        
    });

$("#find_location").on("click", function (e) {
    e.preventDefault();
    $.get("https://ipinfo.io", function (response) {
        $("#wo").each(function () {
            $(this).val(response.city);
        });
    }, "jsonp");
});
$("#find_location_product").on("click", function (e) {
    e.preventDefault();
    $.get("https://ipinfo.io", function (response) {
        $("#wo").each(function () {
            $(this).val(response.city);
        });
    }, "jsonp");
});


$("#result_check").click(function(){


    ip=$("#ip_item:checked").length;
    
    analytic=$("#analytic_item:checked").length;
       

    $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'POST',
                contentType:'application/json; charset=utf-8',
                url:'/ajax/analytics/iniaz/client/'+ip+'/'+analytic,
                data: {filed1:ip,filed2:analytic} ,
                beforeSend:function(){
                    
                },
                success: function (response) {
                    console.log(response.data);
                      console.log("ok");
                     
                        $("#cookieConsent").fadeOut(200);
                     
                   

                },
                error: function (xhr,ajaxOptions,thrownError) {
                    console.log(response.data);
                    $("#cookieConsent").fadeOut(200);
                    console.log(xhr,ajaxOptions,thrownError);
                },
                complete:function(){
                  
                }


            });
    

});



           
                $('#form-login').submit(function (event) {
                var items = $(this).serialize();

                event.preventDefault(event);
        msg_error="{{__('cms.invalid-data')}}";
        msg_error_server="{{__('cms.msg_error_server')}}";
        msg_success_server="{{__('cms.msg_success_server')}}";

            $user=$('input#email').val( );
            $pass=$('input#password').val( );
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'POST',
                url: $(this).attr('action'),
                data:items,
                dataType: "json",
                beforeSend:function(){

                    $('#btn-login').hide();
                    $(".main-register").attr('style','opacity:0.6')
                },
                success: function (response) {
                    if(response.result.status){
                        console.log(response.result.status);
                        $("#result-ajax").empty()
                            .append("<span style='color:green'>"+msg_success_server+"</span>");

                    
                        window.location.href = '/user/panel/dashboard'
                    }
                    else {
                       
                        $("#result-ajax").empty()
                            .append("<span>"+msg_error_server+"</span>");
                    }

                },
                error: function (xhr,ajaxOptions,thrownError) {
                 
                    $("#result-ajax").empty()
                            .append("<span>"+msg_error+"</span>");
                            $('#btn-login').show();
                    $(".main-register").attr('style','opacity:1')
                },
                complete:function(){
                    
                    $('#btn-login').show();
                    $(".main-register").attr('style','opacity:1')
                }
     

            });

            });
            $('#form-register').submit(function (event) {
                var items = $(this).serialize();
                msg_success_server="{{__('cms.msg_success_server')}}";
                msg_success_register_server="{{__('cms.msg_success_register_server')}}";
                msg_error_server="{{__('cms.msg_error_server')}}";

                event.preventDefault(event);

            $.ajax({
                headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
},
                type:'POST',
                url: $(this).attr('action'),
                data:items,
                dataType: "json",
                beforeSend:function(){
                    $('#btn-register').hide();
                    $(".main-register").attr('style','opacity:0.6');
                },
                success: function (response) {
                    console.log(response.result);
                    


                    if(response.result.status){
      
                       if(response.result.validate_error){
                        $("#result-ajax").empty();
                                jQuery.each(response.result.errors, function(index, itemData) {
                                    $("#result-ajax").append("<span>"+itemData+"</span><br>");
                                        });
                                    

                       }
                                
                            
                            else {
                                $("#tab-2").hide();
                                $("#result-ajax").empty()
                            .append("<span style='color:green'>"+msg_success_server+"</span>");
                            window.location.href = '/welcome/register/'+response.result.secret
                               
                              
                                    
                            }
                    }

                   

                },
                error: function (xhr,ajaxOptions,thrownError) {
                    
                    $("#result-ajax").empty()
                            .append("<span>"+msg_error_server+"</span>");
                            $('#btn-login').show();
                    $(".main-register").attr('style','opacity:1')
                    $('#btn-register').show();
                 
                },
                complete:function(){
                    $('#btn-register').show();
                    $(".main-register").attr('style','opacity:1')
                }


            });

            });

            
                $('#form-sendcode').submit(function (event) {

                    var items = $(this).serialize();
               

                event.preventDefault(event);

                msg_error_server="{{__('cms.msg_error_server')}}";
                invalid_email="{{__('cms.invalid-email')}}";
                msg="{{__('cms.enter-your-verify-code')}}";
                text="{{__('cms.please-check-your-email-for-verify-account-right-now')}}";
                btn="{{__('cms.verify-code')}}";
                $email=$('input#email_f').val( );
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                url: $(this).attr('action'),
                data:items,
                dataType: "json",

                beforeSend:function(){
                    $('#btn-sendCode').hide();
                    $('#btn-register').hide();
                    $(".main-register").attr('style','opacity:0.6');
                },
                    success: function (response) {
                    
                        if(response.data.result){

                        $(".box-email").empty()
                        .append("<input id='code_verify' onClick='this.select()' placeholder='"+msg+"' type='text' class='form-control' autocomplete='off'>");
                        $("#result-ajax").empty()
                        .append("<span style='color:green'>"+text+"</span>");
                        $("#code_hidden").attr('data-client',$email)

                        $('#btn-sendCode').html(btn)
                        .attr('onclick','VerifyCode()')
                        }
                        else{
                            $("#result-ajax").empty()
                            .append("<span>"+invalid_email+"</span>");
                        }
                    },
                    error: function (xhr,ajaxOptions,thrownError) {
                        $("#result-ajax").empty()
                            .append("<span>"+msg_error_server+"</span>");
                            $('#btn-sendCode').show();
                            $(".main-register").attr('style','opacity:1')
                    $('#btn-register').show();
                    },
                    complete:function(){
                    $('#btn-sendCode').show();
                    $('#btn-register').show();
                    $(".main-register").attr('style','opacity:1')
                }


                });

                });

        function VerifyCode() {
            msg_success_server="{{__('cms.msg_success_server')}}";
                msg="{{__('cms.invalid-code')}}";
                $email=$('span#code_hidden').data('client');
                $code=$('input#code_verify').val();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                    contentType:'application/json; charset=utf-8',
                    url:'/ajax/email/login/verify/'+$email+"/"+$code,
                    data: { field1:$email,field2:$code} ,
                    beforeSend:function(){
                    $('#btn-sendCode').hide();
                },
                    success: function (response) {

                        if(response.data.result){
                            $("#result-ajax").empty()
                            .append("<span style='color:green'>"+msg_success_server+"</span>");

                            window.location.href = 'user/panel/otp/reset/password/'+response.data.secret

                            
                        }
                       
                    },
                    error: function (xhr,ajaxOptions,thrownError) {
                    
                       console.log(xhr,ajaxOptions,thrownError);
                      
                    },
                    complete:function(){
                    $('#btn-sendCode').show();
                }

                });

                }

                    $("#show-law").click(function(event){
                        event.preventDefault();
                        $("#tab-1").hide();
                        $("#tab-2").hide();
                        $("#tab-3").hide();

                        $("#tab-4").show();

                        $("#tab-menu-login li").removeClass('current');
                        $("#law-tab").addClass('current');

                    });
                    $("#return-register").click(function(event){
                        event.preventDefault();
                        $("#tab-1").hide();
                        $("#tab-4").hide();
                        $("#tab-3").hide();

                        $("#tab-2").show();

                        $("#tab-menu-login li").removeClass('current');
                        $("#register-tab").addClass('current');

                    });
               


        </script>

    </body>
</html>