<section class="team_members">
    <div class="container triangles-of-section">
        <div class="triangle-up-left"></div>
        <div class="square-left"></div>
        <div class="triangle-up-right"></div>
        <div class="square-right"></div>
    </div>
    <div class="container">
        <h2 class="section_header fancy centered">کارمندان </h2>
        <div class="row">
            @foreach($members as $member)
                <div class="col-sm-6 col-md-3">
                    <div class="team_member">
                        <figure style="background-image: url(images/1b.jpg)">
                            @if(!$member->Hasmedia('images'))
                                <img src="{{asset('img/no-img.gif')}}" width="150" height="150"  alt="1a">
                            @else
                                <img src="{{$member->getFirstMediaUrl('images')}}" width="150" height="150" alt="1a">
                            @endif
                        </figure>
                        <h5>{{$member->full_name}}</h5>
                        <small>{{$member->role_name}}</small>
                        <hr>
                        <div class="team_social"> <a href="https://www.facebook.com/mihanwebmaster"><i class="fa fa-facebook"></i></a> <a href="https://twitter.com/mihanwebmaster"><i class="fa fa-twitter"></i></a> <a href="#pinterest"><i class="fa fa-linkedin"></i></a> <a href=""><i class="fa fa-github-alt"></i></a> </div>
                        <p class="short_bio">{{$member->about}} </p>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</section>
