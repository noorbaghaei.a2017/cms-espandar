@extends('template.app')
@section('content')

<div class="content">
                    <!--  section  -->
                    <section class="parallax-section small-par" data-scrollax-parent="true">
                        <div class="bg"  data-bg="{{asset('template/images/bg/hero/4.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <div class="container">
                            <div class="error-wrap">
                                <div class="bubbles">
                                    <h2>404</h2>
                                </div>
                                <p>{{__('cms.sorry')}}</p>
                                <div class="clearfix"></div>
                               
                                <div class="clearfix"></div>
                                
                                <a href="index.html" class="btn   color2-bg">{{__('cms.return')}}<i class="far fa-home-alt"></i></a>
                            </div>
                        </div>
                    </section>
                    <!--  section  end-->
                </div>
   

@endsection

