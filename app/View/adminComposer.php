<?php

namespace App\View;

use Modules\Advertising\Entities\Advertising;
use Modules\Carousel\Entities\Carousel;
use Modules\Client\Entities\Client;
use Modules\Request\Entities\Request as RequestAdmin;
use Modules\Core\Entities\AnalyticsClient;
use Modules\Core\Entities\Category;
use Carbon\Carbon;
use Modules\Core\Entities\Contact;
use Modules\Core\Entities\Country;
use Modules\Core\Entities\Setting;
use Modules\Core\Entities\User;
use Illuminate\View\View;
use Modules\Core\Entities\UserServices;
use Modules\Core\Entities\ListServices;
use Modules\Article\Entities\Article;
use Modules\Comment\Entities\Comment;
use Modules\Information\Entities\Information;
use Modules\Member\Entities\Member;
use Modules\Order\Entities\Order;
use Modules\Product\Entities\Product;

class adminComposer{

    public function compose(View $view){
        $view->with('articlescount', Article::latest()->count());
        $view->with('informationscount', Information::latest()->count());
        $view->with('advertisingscount', Advertising::latest()->count());
        
      
       
        $view->with('deactivebussinesscount', UserServices::latest()->whereIn('status',[0,1])->count());  
        $view->with('nowbussinesscount', UserServices::latest()->whereDate('created_at', Carbon::today())->count());  
        $view->with('requestbussinesscount', UserServices::latest()->whereStatus(0)->count());   
     
        $view->with('total_orders',Order::where('status',1)->sum('total_price'));
        $view->with('carouselscount', Carousel::latest()->count());
       
        
       
       
        $view->with('clients', Client::latest()
            ->whereHas('role',function ($query){
                $query->where('title', '=', 'user');
            })
            ->get());
        $view->with('contact_count', Contact::latest()->count());
        $view->with('userscount', User::latest()->count());
        $view->with('lastArticles', Article::select('id', 'title','text','excerpt','token')->latest()->take(5)->get());
        $view->with('lastUsers', User::latest()->take(5)->get());

    
    }

}
