<?php

namespace App\View;


use Illuminate\View\View;
use Modules\Core\Entities\ListServices;
use Modules\Service\Entities\Service;
use Modules\Brand\Entities\Brand;
use Modules\Core\Entities\UserServices;



class frontComposer{


    public function compose(View $view){


       
      
        // $view->with('all_attributes', AttributeJob::select('id','title','token')->latest()->get());
        // $view->with('requestbussinesscount', UserServices::latest()->count());   
        // $view->with('all_category_advertisings', Category::with('advertisings')->orderBy('order','asc')->where('model',Advertising::class)->whereParent(0)->get());
        // $view->with('all_category_bussiness', ListServices::orderBy('order','asc')->get());
           $view->with('services', Service::orderBy('order','asc')->get());
           $view->with('brandes', Brand::orderBy('order','asc')->get());

        // $view->with('all_parent_category_bussiness', ListServices::orderBy('order','asc')->where('parent',0)->get());
        // $view->with('all_parent_category_products', Category::orderBy('order','asc')->where('model','Modules\Product\Entities\Product')->where('parent',0)->get());
        // $view->with('all_parent_category_products_child', Category::orderBy('order','asc')->where('model','Modules\Product\Entities\Product')->where('parent','<>',0)->get());
        // $view->with('all_category_products_child', Category::orderBy('order','asc')->where('model','Modules\Product\Entities\Product')->where('parent','<>',0)->get());
        // $view->with('all_city_bussiness', UserServices::latest()->whereStatus(2)->get()->pluck('city')->unique()->toArray());
        // $view->with('all_bussiness', UserServices::with('comments','user_info','attributes')->whereStatus(2)->get());       
        // $view->with('all_bussiness_count', UserServices::latest()->with('comments','user_info','attributes')->whereStatus(2)->take(10)->get());       
        $view->with('all_bussiness_special', UserServices::with('list_category')->select('id','service','title','text','postal_code','city','slug','address')->whereStatus(2)->where('is_special',1)->take(10)->get());       
        // $view->with('all_bussiness_special_count', UserServices::latest()->with('comments','user_info','attributes')->whereStatus(2)->where('is_special',1)->count());       
        // $view->with('all_bussiness_category_count', UserServices::latest()->with('comments','user_info','attributes')->whereStatus(2)->take(8)->get());       
        // $view->with('all_category_products', Category::with('products')->orderBy('order','asc')->where('model',Product::class)->whereParent(0)->get());
        // $view->with('all_category_galleries', Category::with('galleries')->orderBy('order','asc')->where('model',Gallery::class)->whereParent(0)->get());
        // $view->with('category_advertisings', Category::with('advertisings')->orderBy('order','asc')->where('model',Advertising::class)->whereParent(0)->take(4)->get());
        // $view->with('category_companies', Category::orderBy('order','asc')->where('model',Client::class)->whereParent(0)->get());
        // $view->with('all_category_advertisings', Category::with('advertisings')->orderBy('order','asc')->where('model',Advertising::class)->whereParent(0)->get());
        // $view->with('original_category_products', Category::with('advertisings')->orderBy('order','asc')->where('model',Product::class)->whereParent(0)->get());
       
        // $view->with('advertisings', Advertising::where('status',1)->get());
    
        // $view->with('advertisingscount', Advertising::latest()->count());
       
       
        $view->with('more_view_category_bussiness', ListServices::select('id','title','slug','status','color','parent','icon','token')->with('user_services')
            ->whereHas('user_services',function($query){
                    $query->whereStatus(2);
            })->get()->sortByDESC(function($query){
                return $query->user_services->count();
            })->take(5));

          
            $view->with('more_view_list_bussiness', UserServices::select('id','service','user','text','excerpt','postal_code','city','token','slug','status','banner_status','email','phone','address','is_special','title')->with('comments','user_info','attributes','list_category')->whereStatus(2)->get()->sortByDESC(function($query){
                return $query->analyzer->view;
            })->take(15));

        // $view->with('popular_articles', Article::get()->sortByDESC(function($query){
        //     return $query->analyzer->view;
        // })->take(4));
        // $view->with('popular_products', Product::get()->sortByDESC(function($query){
        //     return $query->analyzer->view;
        // })->take(4));
     
        // $view->with('listmenus', ListMenu::select('id','label','token','status','name')->latest()->get());
        // $view->with('sliders', Carousel::orderBy('order','asc')->get());
        // $view->with('contact_count', Contact::latest()->where('seen',0)->count());
        // $view->with('products', Product::with('attributes','properties')->take(6)->get());
        // $view->with('new_products', Product::with('attributes','properties')->whereStatus(1)->take(4)->get());
        // $view->with('special_products', Product::with('attributes','properties')->whereSpecial(2)->take(4)->get());
        // $view->with('new_articles', Article::with('user_info')->orderBy('created_at','desc')->take(3)->get());
        // $view->with('articles', Article::with('user_info')->orderBy('created_at','desc')->take(6)->get());
        // $view->with('services', Service::orderBy('created_at','desc')->whereParent(0)->get());
        // $view->with('user_services', UserServices::all());

    }

}
