<?php

namespace App\View;


use App\Events\Order;
use Illuminate\View\View;
use Modules\Core\Entities\ListServices;

use Carbon\Carbon;



class generalCategoryJobAdminComposer{


    public function compose(View $view){

  
        $view->with('categoriescount', ListServices::latest()->count());  

    }

}
