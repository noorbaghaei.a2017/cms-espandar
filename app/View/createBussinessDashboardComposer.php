<?php

namespace App\View;



use Illuminate\View\View;

use Carbon\Carbon;
use Modules\Core\Entities\ListServices;
use Modules\Advertising\Entities\AttributeJob;



class createBussinessDashboardComposer{


    public function compose(View $view){

        $view->with('all_parent_category_bussiness', ListServices::orderBy('order','asc')->where('parent',0)->get());
        $view->with('all_category_bussiness', ListServices::orderBy('order','asc')->get());
        $view->with('all_attributes', AttributeJob::select('id','title','token')->latest()->get());

     
    }

}

