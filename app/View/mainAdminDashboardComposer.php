<?php

namespace App\View;



use Illuminate\View\View;
use Carbon\Carbon;
use Modules\Comment\Entities\Comment;
use Modules\Core\Entities\ListServices;




class mainAdminDashboardComposer{


    public function compose(View $view){

        $view->with('categoriescount', ListServices::latest()->count()); 
        $view->with('commentlast', Comment::select('id','status')->take(5)->get());  
       
    }

}
