<?php

namespace App\View;



use Illuminate\View\View;

use Carbon\Carbon;
use Modules\Core\Entities\Category;
use Modules\Product\Entities\Product;



class createProductDashboardComposer{


    public function compose(View $view){

        $view->with('all_parent_category_products', Category::orderBy('order','asc')->where('model','Modules\Product\Entities\Product')->where('parent',0)->get());
        $view->with('all_category_products_child', Category::orderBy('order','asc')->where('model','Modules\Product\Entities\Product')->where('parent','<>',0)->get());
        $view->with('all_category_products', Category::with('products')->orderBy('order','asc')->where('model',Product::class)->whereParent(0)->get());

    }

}

