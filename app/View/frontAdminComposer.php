<?php

namespace App\View;



use Illuminate\View\View;
use Carbon\Carbon;
use Modules\Request\Entities\Request as RequestAdmin;
use Modules\Product\Entities\Product;
use Modules\Core\Entities\AnalyticsClient;
use Modules\Client\Entities\Client;
use Modules\Core\Entities\ListServices;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\UserServices;
use Modules\Advertising\Entities\Advertising;




class frontAdminComposer{


    public function compose(View $view){

        $view->with('requestscount', RequestAdmin::latest()->count());
        $view->with('reserve_products_count', Product::with('attributes','properties')->whereUser(null)->count());
        $view->with('reserve_jobs_count', UserServices::with('attributes')->whereUser(null)->count());
        $view->with('reserve_advertisings_count', Advertising::whereClient(null)->count());
        $view->with('ipviewscount', AnalyticsClient::latest()->count());
        $view->with('productscount', Product::latest()->where('user','<>',null)->whereStatus(2)->count());
        $view->with('categoriesproductcount', Category::latest()->where('model','Modules\Product\Entities\Product')->count());  
        $view->with('clientscount', Client::latest()
        ->whereHas('role',function ($query){
            $query->where('title', '=', 'user');
        })->where('is_verify_email',1)
        ->count());
        $view->with('all_bussiness_special_count', UserServices::latest()->with('comments','user_info','attributes')->whereStatus(2)->where('is_special',1)->count());       
        $view->with('bussinesscount', UserServices::latest()->whereStatus(2)->where('user','<>',null)->count());  
        $view->with('popular_bussiness', UserServices::whereStatus(2)->get()->sortByDESC(function($query){
            return $query->analyzer->view;
        })->take(4));
       

    }

}
