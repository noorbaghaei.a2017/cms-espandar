<?php
return [
    "text-create"=>"you can create your videos",
    "text-edit"=>"you can edit your videos",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"videos list",
    "singular"=>"video",
    "collect"=>"videos",
    "permission"=>[
        "videos-full-access"=>"videos full access",
        "videos-list"=>"videos list",
        "videos-delete"=>"videos delete",
        "videos-create"=>"videos create",
        "videos-edit"=>"edit videos",
    ]
];
