<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'), "middleware" => ["auth:web"]], function () {
    Route::resource('/articles', 'ArticleController')->only('create','store','destroy','update','index','edit');
    Route::get('articles/{lang}/{token}', 'ArticleController@languageShow')->name('article.language.show');
    Route::patch('articles/lang/update/{lang}/{token}', 'ArticleController@languageUpdate')->name('article.language.update');

    Route::group(["prefix"=>'search'], function () {
        Route::post('/articles', 'ArticleController@search')->name('search.article');
    });


    Route::group(["prefix"=>'article/categories'], function () {
        Route::get('/', 'ArticleController@categories')->name('article.categories');
        Route::get('/create', 'ArticleController@categoryCreate')->name('article.category.create');
        Route::post('/store', 'ArticleController@categoryStore')->name('article.category.store');
        Route::get('/edit/{category}', 'ArticleController@categoryEdit')->name('article.category.edit');
        Route::patch('/update/{category}', 'ArticleController@categoryUpdate')->name('article.category.update');

    });

    Route::group(["prefix"=>'article/questions'], function () {
        Route::get('/{article}', 'ArticleController@question')->name('article.questions');
        Route::get('/create/{article}', 'ArticleController@questionCreate')->name('article.question.create');
        Route::post('/store/{article}', 'ArticleController@questionStore')->name('article.question.store');
        Route::delete('/destroy/{question}', 'ArticleController@questionDestroy')->name('article.question.destroy');
        Route::get('/edit/{article}/{question}', 'ArticleController@questionEdit')->name('article.question.edit');
        Route::patch('/update/{question}', 'ArticleController@questionUpdate')->name('article.question.update');
    });

});
