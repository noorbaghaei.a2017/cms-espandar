<?php

namespace Modules\Article\Transformers;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Article\Entities\Article;

class ArticleCollection extends ResourceCollection
{
    public $collects = Article::class;


    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $item ) {
                    return new ArticleResource($item);
                }
            ),
            'filed' => [
                'thumbnail','title','slug','view','like','questions','update_date','create_date'
            ],
            'public_route'=>[

                  [
                      'name'=>'articles.create',
                      'param'=>[null],
                      'icon'=>config('cms.icon.add'),
                      'title'=>__('cms.add'),
                      'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                      'method'=>'GET',
                  ],
                [
                    'name'=>'article.categories',
                    'param'=>[null],
                    'icon'=>config('cms.icon.categories'),
                    'title'=>__('cms.categories'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-warning pull-right',
                    'method'=>'GET',
                ]
            ],
            'private_route'=>
               [
                   [
                       'name'=>'articles.edit',
                       'param'=>[
                               'article'=>'token'
                       ],
                       'icon'=>config('cms.icon.edit'),
                       'title'=>__('cms.edit'),
                       'class'=>'btn btn-warning btn-sm text-sm',
                       'modal'=>false,
                       'method'=>'GET',
                   ],
                   [
                       'name'=>'articles.destroy',
                       'param'=>[
                               'article'=>'token'
                       ],
                       'icon'=>config('cms.icon.delete'),
                       'title'=>__('cms.delete'),
                       'class'=>'btn btn-danger btn-sm text-sm text-white',
                       'modal'=>true,
                       'modal_type'=>'destroy',
                       'method'=>'DELETE',
                   ],
                   [
                       'name'=>'articles.detail',
                       'param'=>[null],
                       'icon'=>config('cms.icon.detail'),
                       'title'=>__('cms.detail'),
                       'class'=>'btn btn-primary btn-sm text-sm text-white',
                       'modal'=>true,
                       'modal_type'=>'detail',
                       'method'=>'GET',
                   ]
               ],
            'search_route'=>[

                'name'=>'search.article',
                'filter'=>[
                    'title','slug'
                ],
                'icon'=>config('cms.icon.add'),
                'title'=>__('cms.add'),
                'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                'method'=>'POST',

            ],
            'language_route'=>true,
            'class_model'=> 'article',
            'count' => $this->collection->count(),
            'links' => [
                'self' => 'link-value',
            ],
            'collect'=>__('article::articles.collect'),
            'title'=>__('article::articles.index'),
        ];
    }
}
