<?php

namespace Modules\Carousel\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Translate;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Permission\Models\Role;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Carousel extends Model implements HasMedia
{

   
    use HasMediaTrait,TimeAttribute,QueryCacheable;

    protected $cacheFor = 360; // 1 minutes

    protected $fillable = ['title','excerpt','text','token','user','order'];


    public function getRouteKeyName()
    {
        return multiRouteKey();
    }

    // public function translates()
    // {
    //     return $this->morphMany(Translate::class, 'translateable');
    // }
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(200)
            ->height(200)
            ->performOnCollections(config('cms.collection-image'));
    }



}
