

@extends('core::dashboard.main')

@section('content')
   
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('cms.slider')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.slider')}}</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- /.container-fluid -->


  <!-- Main content -->
  <section class="content">
      <div class="row">
      
      <div class="col-md-12">

      <div class="card">
              <div class="card-header">
              <a href="{{route('carousels.create')}}" class="btn btn-info"><i class="fas fa-plus"></i></a>
<br>
<br>
                <h3 class="card-title">{{__('cms.slider')}}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr><th colspan="6">{{__('cms.slider')}}</th></tr>

                      <tr>
                      <th style="width: 10px">#</th>
                      <th>{{__('cms.thumbnail')}}</th>
                      <th>{{__('cms.title')}}</th>
                     
                      <th style="width: 40px">{{__('cms.status')}}</th>
                      <th>{{__('cms.option')}}</th>

                      </tr>
                  </thead>
                  <tbody>
                  @foreach ($items as $key=>$item)
                 
                  <tr>
                  <td>{{++$key}}</td>
                      <td>
                      @if($item->Hasmedia('images'))

<img src="{{$item->getFirstMediaUrl('images')}}" width="50" style="object-fit:cover;height: 53px;">

                      @else
                      
<img src="{{asset('template/images/no-image.jpg')}}" width="50" style="object-fit:cover;height: 53px;">

                      @endif
                      </td>
                      <td>{{$item->title}}</td>
                   
                      <td><span class="badge bg-success">{{__('cms.active')}}</span></td>
                      <td class="text-left py-0 align-middle">
                      <div class="btn-group btn-group-sm">
                        <a href="{{route('carousels.edit',['carousel'=>$item->token])}}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                        
                      </div>
                    </td>
</tr>

                  @endforeach
                    
                 
                 
                  </tbody>
                  <tfoot>
                  <tr>
                  <th style="width: 10px">#</th>
                      <th>{{__('cms.thumbnail')}}</th>
                      <th>{{__('cms.title')}}</th>
                     
                      <th style="width: 40px">{{__('cms.status')}}</th>
                      <th>{{__('cms.option')}}</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>


     
        </div>
      </div>
    
    </section>
    <!-- /.content -->
 
  


    


@endsection


@section('scripts')

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>



@endsection












