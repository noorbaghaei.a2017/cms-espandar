

    <li>
        <a href="{{route('races.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('educational.icons.race')}}"></i>
                              </span>
            <span class="nav-text"> {{__('educational::races.collect')}}</span>
        </a>
    </li>

