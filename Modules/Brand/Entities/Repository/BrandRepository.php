<?php


namespace Modules\Brand\Entities\Repository;


use Modules\Brand\Entities\Brand;

class BrandRepository implements BrandRepositoryInterface
{

    public function getAll()
    {
        return Brand::latest()->get();
    }
}
