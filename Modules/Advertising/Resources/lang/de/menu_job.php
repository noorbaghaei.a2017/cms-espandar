<?php
return [
    "text-create"=>"you can create your menu job",
    "text-edit"=>"you can edit your menu job",
    "store"=>"Store Success",
    "store_cv"=>"Send Cv",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"menus job list",
    "error"=>"error",
    "singular"=>"menu job",
    "collect"=>"menus job",
    "permission"=>[
        "menu_job-full-access"=>"menu job full access",
        "menu_job-list"=>"menus job list",
        "menu_job-delete"=>"menu job delete",
        "menu_job-create"=>"menu job create",
        "menu_job-edit"=>"edit menu job",
    ]


];
