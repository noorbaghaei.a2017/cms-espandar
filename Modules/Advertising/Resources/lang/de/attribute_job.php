<?php
return [
    "text-create"=>"you can create your attribute job",
    "text-edit"=>"you can edit your attribute job",
    "store"=>"Store Success",
    "store_cv"=>"Send Cv",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"attributes job list",
    "error"=>"error",
    "singular"=>"attribute job",
    "collect"=>"attributes job",
    "permission"=>[
        "attribute_job-full-access"=>"attribute job full access",
        "attribute_job-list"=>"attributes job list",
        "attribute_job-delete"=>"attribute job delete",
        "attribute_job-create"=>"attribute job create",
        "attribute_job-edit"=>"edit attribute job",
    ]


];
