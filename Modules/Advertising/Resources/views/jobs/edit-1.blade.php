@extends('core::layout.panel')
@section('pageTitle', __('cms.edit'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box p-a-xs">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#">
                            @if(!$item->Hasmedia('images'))
                                    <img style="width: 400px;height: auto" src="{{asset('img/no-img.gif')}}" alt="" class="img-responsive">



                            @else
                                    <img style="width: 400px;height: auto" src="{{$item->getFirstMediaUrl('images')}}" alt="" class="img-responsive">

                                @endif


                            </a>
                        </div>
                        <div class="col-md-7">
                        <div style="padding-top: 35px">
                            @foreach(config('cms.extra-lang') as $language)
                                <a href="{{route('job.language.show',['lang'=>$language,'token'=>$item->token])}}">
                                    <img src="{{asset(config('cms.flag.'.$language))}}" width="30">
                                </a>
                            @endforeach
                        </div>
                            <div style="padding-top: 35px">
                                <h6 style="padding-top: 35px"> {{__('cms.subject')}} : </h6>
                                <h4 style="padding-top: 35px">    {{$item->title}}</h4>
                            </div>
                            <div>
                                <h6 style="padding-top: 35px"> {{__('cms.excerpt')}} : </h6>
                                <p>    {{$item->excerpt}}</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @include('core::layout.alert-danger')
                    <div class="box-header">
                        <div class="pull-left">
                            <small>
                                {{__('advertising::jobs.text-edit')}}
                            </small>
                        </div>
                        <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white pull-right">{{__('cms.print')}} </a>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <form id="signupForm" action="{{route('jobs.update', ['job' => $item->token])}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" value="{{$item->token}}" name="token">
                            {{method_field('PATCH')}}
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="title" class="form-control-label">{{__('cms.title')}}</label>
                                <input type="text" name="title" class="form-control" id="title"  value="{{$item->title}}">
                            </div>
                            <div class="col-sm-6">
                                <label for="title" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                @include('core::layout.load-single-image')
                            </div>

                        </div>
                        <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="mobile" class="form-control-label">{{__('cms.mobile')}} </label>
                                    <input type="text" name="mobile" value="{{$item->mobile}}" class="form-control" id="mobile" required autocomplete="off">
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="phone" class="form-control-label">{{__('cms.phone')}} </label>
                                    <input type="text" name="phone" value="{{$item->phone}}" class="form-control" id="phone" required autocomplete="off">
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="email" class="form-control-label">{{__('cms.email')}} </label>
                                    <input type="text" name="email" value="{{$item->email}}" class="form-control" id="email" required autocomplete="off">
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="address" class="form-control-label">{{__('cms.address')}} </label>
                                    <input type="text" name="address" value="{{$item->address}}" class="form-control" id="address" required autocomplete="off">
                                </div>
                              


                            </div>
                            <div class="form-group row">
                            <div class="col-sm-6">
                                    <span class="text-danger">*</span>
                                    <label for="postal_code" class="form-control-label">{{__('cms.postal_code')}} </label>
                                    <input  onkeyup="loadCity(event)" type="text" name="postal_code" value="{{$item->postal_code}}" class="form-control" id="postal_code" required autocomplete="off">
                                </div>

                                <div class="col-sm-6">
                                <span class="text-danger">*</span>
                                <label for="city" class="form-control-label">{{__('cms.cities')}}  </label>
                                <select dir="ltr" class="form-control" id="city" name="city" required>
                                   
                                       
                                       <option vlaue="{{$item->city}}" selected>{{$item->city}}</option>
                                    

                                </select>
                            </div>

                            </div>



                            <div class="form-group row">

<div class="col-sm-12">
    <span class="text-danger">*</span>
    <label for="list_services" class="form-control-label">{{__('cms.category')}}  </label>
    <select dir="ltr" class="form-control" id="list_services" name="list_services" required>
        @foreach($list_services as $list_service)
            <option value="{{$list_service->id}}"  {{$item->service==$list_service->id ? 'selected' : ''}}>{{$list_service->title}}</option>
        @endforeach

    </select>
</div>

</div>



<div class="form-group row">
                            <div class="col-sm-12">

<textarea class="form-control" name="text" col="10" row="10">
{{$item->text}}
</textarea>
                            </div>
                            </div>
                           
                           
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <span class="text-danger">*</span>
                                    <label for="excerpt" class="form-control-label">{{__('cms.excerpt')}} </label>
                                    <input type="text" value="{{$item->excerpt}}" name="excerpt" class="form-control" id="excerpt" autocomplete="off">
                                </div>


                            </div>

                           

                            @include('core::layout.update-button')

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>



function loadCity(e) {

var myInput = document.getElementById("postal_code");
    


if(myInput.value.length==5){

    $code=$("#postal_code").val();


$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/city/postalcode/'+$code,
    data: { field1:$code} ,
    beforeSend:function(){
    
},
    success: function (response) {
    if(response.data.status){

        console.log(response.data);
        $('#city').empty();
       
            $('#city').append("<option value='"+response.data.result[0].fields.plz_name+"' selected>"+response.data.result[0].fields.plz_name+"</option>");
        
            

            

    }
    else{
        console.log('no');
    }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
        // $("#result-ajax").empty()
        //     .append("<span>"+msg+"</span>");
    },
    complete:function(){

}

});

}
}




        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    title: {
                        required: true
                    },
                    text: {
                        required: true
                    },
                    excerpt: {
                        required: true
                    },
                    category: {
                        required: true
                    },
                    status: {
                        required: true
                    },


                },
                messages: {
                    title:"{{__('cms.title.required')}}",
                    text: "{{__('cms.text.required')}}",
                    excerpt: "{{__('cms.excerpt.required')}}",
                    category: "{{__('cms.category.required')}}",
                    status: "{{__('cms.status.required')}}",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
