@extends('core::dashboard.main')

@section('content')
   



    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('cms.advertisings')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.advertisings')}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">


               
                    <img id="show-image" class="profile-user-img img-fluid img-circle"
                       src="{{asset('template/images/no-image.jpg')}}"
                       alt="" style="border-radius:unset;width:100%;">

    
                 


                </div>

                <h3 class="profile-username text-center">{{old('title')}}</h3>

                <p class="text-muted text-center">Manager</p>

               

               

             
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{__('cms.about-it')}}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                 
                  <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">{{__('cms.setting')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#privacy" data-toggle="tab">{{__('cms.privacy_info')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#translate" data-toggle="tab">{{__('cms.translate')}}</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
              @include('core::layout.alert-danger')
              @include('core::layout.alert-success')
                <div class="tab-content">
                 
                 

                  <div class="tab-pane active" id="settings">
                    <form action="{{route('advertisings.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                
                    <div class="form-group row">
                    <div class="col-sm-10">
                      <input name="image" type="file" onchange="loadFile(event)" class="custom-file-input" id="customFile">
                      <label class="custom-file-label" for="customFile">Choose Image</label>
                    </div>
                    </div>  
                  
                    
                      <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">{{__('cms.title')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="title" value="{{old('title')}}" class="form-control" id="title" placeholder="{{__('cms.title')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="text" class="col-sm-2 col-form-label">{{__('cms.text')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="text" value="{{old('text')}}" class="form-control" id="text" placeholder="{{__('cms.text')}}">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="excerpt" class="col-sm-2 col-form-label">{{__('cms.excerpt')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="excerpt" value="{{old('excerpt')}}" class="form-control" id="excerpt" placeholder="{{__('cms.excerpt')}}">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">{{__('cms.email')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="email" value="{{old('email')}}" class="form-control" id="email" placeholder="{{__('cms.email')}}">
                        </div>
                      </div>
                     
                     

                      <div class="form-group row">
                        <label for="gender" class="col-sm-2 col-form-label">{{__('cms.gender')}}</label>
                        <div class="col-sm-10">
                        <select name="gender" class="form-control">
                        <option  value="1" selected>{{__('cms.lady')}}</option>
                                        <option  value="2">{{__('cms.sir')}}</option>
                                        <option  value="3">{{__('cms.no-matter')}}</option>
                        </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="status" class="col-sm-2 col-form-label">{{__('cms.status')}}</label>
                        <div class="col-sm-10">
                        <select name="status" class="form-control">
                        <option  value="1" selected>{{__('cms.active')}}</option>
                                        <option  value="0">{{__('cms.inactive')}}</option>
                        </select>
                        </div>
                      </div>


                      <div class="form-group row">
                        <label for="type_salary" class="col-sm-2 col-form-label">{{__('cms.type')}}</label>
                        <div class="col-sm-10">
                        <select name="type_salary" class="form-control">
                        
                      <option value="yearly" selected>{{__('cms.yearly')}}</option>
                      <option value="monthly" >{{__('cms.monthly')}}</option>
                      <option value="daily" >{{__('cms.daily')}}</option>
                      <option value="daily" >{{__('cms.hourly')}}</option>
                      <option value="daily" >{{__('cms.project')}}</option>


                        </select>
                        </div>
                      </div>


                      <div class="form-group row">
                        <label for="type" class="col-sm-2 col-form-label">{{__('cms.type')}}</label>
                        <div class="col-sm-10">
                        <select name="type" class="form-control">
                        
                          <option value="employee" selected>{{__('cms.employee')}}</option>
                          <option value="employer" >{{__('cms.employer')}}</option>

                        </select>
                        </div>
                      </div>
                      
                      


                      <div class="form-group row">
                        <label for="special" class="col-sm-2 col-form-label">{{__('cms.special')}}</label>
                        <div class="col-sm-10">
                        <select name="special" class="form-control">
                        <option  value="1" >{{__('cms.active')}}</option>
                                        <option  value="0" selected>{{__('cms.inactive')}}</option>
                        </select>
                        </div>
                      </div>

                      
                     

                      <div class="form-group row">
                        <label for="postal_code" class="col-sm-2 col-form-label">{{__('cms.postal_code')}}</label>
                        <div class="col-sm-10">
                          <input id="postal_code" onkeyup="loadCity(event)" type="text" name="postal_code" value="{{old('postal_code')}}" class="form-control" id="postal_code" placeholder="{{__('cms.postal_code')}}">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="city" class="col-sm-2 col-form-label">{{__('cms.city')}}</label>
                        <div class="col-sm-10">
                        <select id="city"  name="city" class="form-control">
                       
                        </select>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="address" class="col-sm-2 col-form-label">{{__('cms.address')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="address" value="{{old('address')}}" class="form-control" id="address" placeholder="{{__('cms.address')}}">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="phone" class="col-sm-2 col-form-label">{{__('cms.phone')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="phone" value="{{old('phone')}}" class="form-control" id="phone" placeholder="{{__('cms.phone')}}">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.clients')}}</label>
                        <div class="col-sm-10">
                        <select name="client" class="form-control">
                        <option value="0" selected>{{__('cms.empty')}}</option>

@foreach($clients as $client)
<option value="{{$client->token}}">{{$client->full_name}}</option>
@endforeach
                        </select>
                        </div>
                      </div>



                      
                     
                    
                     
                     
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="tab-pane" id="privacy">
                   
                  </div>


                  <div class="tab-pane" id="translate">
                  

                   </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
    </section>





@endsection



@section('scripts')


<script>

function loadFile(e) {
	 image = $('#show-image');
    
    image.attr('src',URL.createObjectURL(event.target.files[0]));
    
};



function loadCity(e) {

var myInput = document.getElementById("postal_code");
    


if(myInput.value.length==5){

    $code=$("#postal_code").val();


$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/city/postalcode/'+$code,
    data: { field1:$code} ,
    beforeSend:function(){

        $('#city').hide();
    
},
    success: function (response) {

       
    if(response.data.status){

        console.log(response.data);
        $('#city').empty();
       
            $('#city').append("<option value='"+response.data.result[0].fields.plz_name+"' selected>"+response.data.result[0].fields.plz_name+"</option>");
        

    }
    else{
        console.log('no');
    }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
        // $("#result-ajax").empty()
        //     .append("<span>"+msg+"</span>");
    },
    complete:function(){
        $('#city').show();
}

});

}
}



</script>

@endsection
 
 
 














