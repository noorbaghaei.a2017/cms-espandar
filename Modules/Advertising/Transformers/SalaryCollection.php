<?php

namespace Modules\Advertising\Transformers;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Advertising\Entities\Salary;

class SalaryCollection extends ResourceCollection
{
    public $collects=Salary::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $item ) {
                    return new SalaryResource($item);
                }
            ),
            'filed' => [
                'title','symbol','slug','order','update_date','create_date'
            ],
            'public_route'=>[

                [
                    'name'=>'salaries.create',
                    'param'=>[null],
                    'icon'=>config('cms.icon.add'),
                    'title'=>__('cms.add'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                    'method'=>'GET',
                ]
            ],
            'private_route'=>
                [
                    [
                        'name'=>'salaries.edit',
                        'param'=>[
                            'salary'=>'token'
                        ],
                        'icon'=>config('cms.icon.edit'),
                        'title'=>__('cms.edit'),
                        'class'=>'btn btn-warning btn-sm text-sm',
                        'modal'=>false,
                        'method'=>'GET',
                    ],
                    [
                        'name'=>'salaries.detail',
                        'param'=>[null],
                        'icon'=>config('cms.icon.detail'),
                        'title'=>__('cms.detail'),
                        'class'=>'btn btn-primary btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'detail',
                        'method'=>'GET',
                    ]
                ],
            'search_route'=>[

                'name'=>'search.experience',
                'filter'=>[
                    'title','symbol','order'
                ],
                'icon'=>config('cms.icon.add'),
                'title'=>__('cms.add'),
                'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                'method'=>'POST',

            ],
            'count' => $this->collection->count(),
            'links' => [
                'self' => 'link-value',
            ],
            'collect'=>__('advertising::salaries.collect'),
            'title'=>__('advertising::salaries.index'),
        ];
    }
}
