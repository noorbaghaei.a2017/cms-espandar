<?php

namespace Modules\Advertising\Transformers;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Core\Entities\ListServices;

class CategoryJobCollection extends ResourceCollection
{
    public $collects=ListServices::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $item ) {
                    return new CategoryJobResource($item);
                }
            ),
            'filed' => [
                'thumbnail','title','slug','update_date','create_date'
            ],
            'public_route'=>[

                [
                    'name'=>'categoryjobs.create',
                    'param'=>[null],
                    'icon'=>config('cms.icon.add'),
                    'title'=>__('cms.add'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                    'method'=>'GET',
                ],
               
            ],
            'private_route'=>
                [
                    [
                        'name'=>'categoryjobs.edit',
                        'param'=>[
                            'categoryjob'=>'token'
                        ],
                        'icon'=>config('cms.icon.edit'),
                        'title'=>__('cms.edit'),
                        'class'=>'btn btn-warning btn-sm text-sm',
                        'modal'=>false,
                        'method'=>'GET',
                    ],
                    [
                        'name'=>'categoryjobs.detail',
                        'param'=>[null],
                        'icon'=>config('cms.icon.detail'),
                        'title'=>__('cms.detail'),
                        'class'=>'btn btn-primary btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'detail',
                        'method'=>'GET',
                    ]
                ],
                
                'language_route'=>true,
                'class_model'=> 'categoryjob',
            'count' => $this->collection->count(),
            'links' => [
                'self' => 'link-value',
            ],
            'collect'=>__('advertising::category_jobs.collect'),
            'title'=>__('advertising::category_jobs.index'),
        ];
    }
}
