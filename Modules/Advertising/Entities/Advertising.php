<?php

namespace Modules\Advertising\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Advertising\Helper\AdvertisingHelper;
use Modules\Client\Entities\Client;
use Modules\Core\Entities\Analyzer;
use Modules\Core\Entities\Translate;
use Modules\Core\Entities\Info;
use Modules\Core\Entities\UserCompany;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Order\Entities\OrderList;
use Modules\Question\Entities\Question;
use Modules\Seo\Entities\Seo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Tags\HasTags;

class Advertising extends Model implements HasMedia
{
    use TimeAttribute,HasTags,HasMediaTrait ,Sluggable;

    protected $fillable = ['title','address','email','mobile','type_salary','postal_code','type','special','client','skill','force','category','display','salary','state','gender','count_member','country','city','education','slug','text','status','excerpt','work_experience','kind','expire','client','plan','number_employees','currency','guild','slug','token','phone','access_email','access_mobile','access_phone','access_address'];

    public function getRouteKeyName()
    {
        return multiRouteKey();
    }
    public function client_info()
    {
        return $this->belongsTo(Client::class, 'client','id')->with('company');
    }

    public  function info(){
        return $this->morphOne(Info::class,'infoable');
    }

    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }

    public function translates()
    {
        return $this->morphMany(Translate::class, 'translateable');
    }

    public function user_info()
    {
        return $this->belongsTo(Client::class,'client','id');
    }


    public function orderList()
    {
        return $this->morphOne(OrderList::class, 'orderable');
    }

    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable');
    }
    public function info_company()
    {
        return $this->hasOne(Client::class, 'id','client');
    }
    public function creator()
    {
        return $this->hasOne(Client::class, 'id','client');
    }
    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }
    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }


    public  function getViewAttribute(){

        return $this->analyzer->view;
    }

    public  function getShowStatusAttribute(){

        return AdvertisingHelper::status($this->status);
    }

    public  function getShowCategoryAttribute(){

        return showCategory($this->category)->symbol;
    }

    public  function getLikeAttribute(){

        return $this->analyzer->like;
    }

    public  function getQuestionAttribute(){

        return $this->questions()->count();
    }

}
