<?php


namespace Modules\Advertising\Entities\Repository;


interface SkillRepositoryInterface
{
    public  function getAll();
}
