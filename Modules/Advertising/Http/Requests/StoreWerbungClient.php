<?php

namespace Modules\Advertising\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\ForiegnPostalCode;
use Illuminate\Http\Request;

class StoreWerbungClient extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'type_salary'=>'required',
            'mobile'=>'required',
            'email'=>'required|email',
            'city'=>'required',
            'postal_code'=>['required','numeric','digits:5',new ForiegnPostalCode()],
            'image'=>'mimes:jpeg,png,jpg|max:3000',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
