<?php

namespace Modules\Advertising\Http\Controllers;

use App\Events\Order;
use Carbon\Carbon;
use Modules\Core\Entities\Setting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Modules\Advertising\Entities\Advertising;
use Modules\Advertising\Entities\Education;
use Modules\Advertising\Entities\Experience;
use Modules\Advertising\Entities\Guild;
use Modules\Advertising\Entities\Period;
use Modules\Advertising\Entities\Position;
use Modules\Advertising\Entities\Repository\AdvertisingRepositoryInterface;
use Modules\Advertising\Entities\Salary;
use Modules\Advertising\Entities\Skill;
use Modules\Advertising\Entities\TypeAdvertising;
use Modules\Advertising\Http\Requests\AdvertisingRequest;
use Modules\Advertising\Transformers\AdvertisingCollection;
use Modules\Client\Entities\Client;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\City;
use Modules\Core\Entities\Country;
use Modules\Core\Entities\Currency;
use Modules\Core\Entities\State;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasGallery;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Plan\Entities\Plan;
use Modules\Plan\Helper\PlanHelper;
use Modules\Service\Entities\Advantage;

class AdvertisingController extends Controller
{
    use HasQuestion,HasCategory,HasGallery;

    protected $entity;
    protected $class;
    protected $query;
    private $repository;


//category

    protected $route_categories_index='advertising::categories.index';
    protected $route_categories_create='advertising::categories.create';
    protected $route_categories_edit='advertising::categories.edit';
    protected $route_categories='advertising.categories';


//question

    protected $route_questions_index='advertising::questions.index';
    protected $route_questions_create='advertising::questions.create';
    protected $route_questions_edit='advertising::questions.edit';
    protected $route_questions='advertising.index';


//gallery

    protected $route_gallery_index='advertising::advertisings.gallery';
    protected $route_gallery='advertisings.index';

//notification

    protected $notification_store='advertising::advertisings.store';
    protected $notification_update='advertising::advertisings.update';
    protected $notification_delete='advertising::advertisings.delete';
    protected $notification_error='advertising::advertisings.error';





    public function __construct()
    {
      
        $this->entity=new Advertising();
        $this->class=Advertising::class;
        $this->middleware('permission:advertising-list');
        $this->middleware('permission:advertising-create')->only(['create','store']);
        $this->middleware('permission:advertising-edit' )->only(['edit','update']);
        $this->middleware('permission:advertising-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        try {

            $items=Advertising::latest()->get();

            return view('advertising::advertisings.index',compact('items'));

        }catch (\Exception $exception){
           
           
            return abort('500');
        }
    }
      /**
     * Display a listing of the resource.
     * @return Response
     */
    public function new()
    {
        try {

            $items=Advertising::where('status',1)->latest()->get();

            return view('advertising::advertisings.index',compact('items'));

        }catch (\Exception $exception){
           
            return abort('500');
        }
    }
      /**
     * Display a listing of the resource.
     * @return Response
     */
    public function reserve()
    {
        try {

            $items=Advertising::where('user',null)->latest()->get();

            return view('advertising::advertisings.index',compact('items'));

        }catch (\Exception $exception){
           
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
          
            $clients=Client::latest()->get();
            return view('advertising::advertisings.create',compact('clients'));
        }catch (\Exception $exception){
           
            return abort('500');
        }
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
            !isset($request->title) &&
            !isset($request->slug)
            ){
                $items=$this->repository->getAll();

                $result = new AdvertisingCollection($items);

                $data= collect($result->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->where("slug",'LIKE','%'.trim($request->slug).'%')
                ->paginate(config('cms.paginate'));
            $result = new AdvertisingCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param AdvertisingRequest $request
     * @return Response
     */
    public function store(AdvertisingRequest $request)
    {
        try {


            DB::beginTransaction();
          

            $this->entity->client=$request->client=='0' ? null : Client::whereToken($request->input('client'))->first()->id;
            $this->entity->title=$request->input('title');
            $this->entity->email=$request->input('email');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->postal_code=$request->input('postal_code');
            $this->entity->type=$request->input('type');
            $this->entity->type_salary=$request->input('type_salary');
            $this->entity->city=$request->input('city');
            $this->entity->phone=$request->input('phone');
            $this->entity->address=$request->input('address');
            $this->entity->country='german';
            $this->entity->text=$request->input('text');
            $this->entity->status=$request->input('status');
            $this->entity->special=$request->input('special');
            $this->entity->gender=$request->input('gender');
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            
            $this->entity->seo()->create([
               
            ]);

           
            $this->entity->analyzer()->create();

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$saved){
                DB::rollBack();
                return redirect()->back()->with('error',__('advertising::advertisings.error'));
            }else{
                DB::commit();
                return redirect(route('advertisings.index'))->with('message',__('advertising::advertisings.store'));
            }


        }catch (\Exception $exception){
          
            DB::rollBack();
    
            return redirect()->back()->with('error',__('advertising::advertisings.error'));

        }

    }

    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $clients=Client::latest()->get();
            $categories=Category::latest()->where('model',Advertising::class)->get();
            $employers=Client::latest()->whereHas('role',function ($query){
                    $query->where('title', '=', 'employer');
                });
            $item=$this->entity->with('tags')->whereToken($token)->first();
            $plans=Plan::latest()->where('status',1)->get();
            $positions=Position::latest()->get();
            $currencies=Currency::latest()->get();
            $educations=Education::latest()->get();
            $types=TypeAdvertising::latest()->get();
            $skills=Skill::latest()->get();
            $salaries=Salary::latest()->get();
            $guilds=Guild::latest()->get();
            $experiences=Experience::latest()->get();
            return view('advertising::advertisings.edit',compact('item','categories','employers','plans','positions','educations','types','salaries','experiences','guilds','currencies','skills','clients'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $token
     * @return Response
     */
    public function update(Request $request, $token)
    {
        try {
            DB::beginTransaction();
            $validator=Validator::make($request->all(),[
                'title'=>'required|string',
                'text'=>'required|string',
                'excerpt'=>'required|string',
                'image'=>'mimes:jpeg,png,jpg|max:2000',
                'kinds'=>'required',
                'skills'=>'required',
                'number_employees'=>'required|numeric',
                'category'=>'required',
                'salary'=>'required',
                'experience'=>'required',
                'positions'=>'required',
                'educations'=>'required',
                'guild'=>'required',
                'currency'=>'required',
                'gender'=>'required',
            ]);
            if($validator->fails()){
                return  redirect()->back()->withErrors($validator);
            }



            $category=Category::whereToken($request->input('category'))->firstOrFail();
            $currency=Currency::whereToken($request->input('currency'))->firstOrFail();

            $this->entity=$this->entity->whereToken($token)->firstOrFail();

            $updated=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "slug"=>null,
                "title"=>$request->input('title'),
                "category"=>$category->id,
                "excerpt"=>$request->input('excerpt'),
                "text"=>$request->input('text'),
                "number_employees"=>$request->input('number_employees'),
                "salary"=>$request->input('salary'),
                "city"=>City::find($request->input('city'))->name,
                "state"=>State::find($request->input('state'))->name,
                "work_experience"=>$request->input('experience'),
                "job_position"=>json_encode($request->input('positions')),
                "status"=>$request->input('status'),
                "country"=>Country::find($request->input('country'))->name,
                "education"=>json_encode($request->input('educations')),
                "guild"=>$request->input('guild'),
                "skill"=>json_encode($request->input('skills')),
                "gender"=>$request->input('gender'),
                "currency"=>$currency->id,
            ]);



            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);



            $this->entity->attachTags($request->input('tags'));

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$updated){
                DB::rollBack();
                return redirect()->back()->with('error',__('advertising::advertisings.error'));
            }else{
                DB::commit();
                return redirect(route("advertisings.index"))->with('message',__('advertising::advertisings.update'));
            }
        }catch (\Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    public  function languageShow(Request $request,$lang,$token){
        $item=Advertising::with('translates')->where('token',$token)->first();
        return view('advertising::werbungs.language',compact('item','lang'));

    }
    public  function languageUpdate(Request $request,$lang,$token){

        DB::beginTransaction();
        $item=Advertising::with('translates')->where('token',$token)->first();
        if( $item->translates->where('lang',$lang)->first()){
            $changed=$item->translates->where('lang',$lang)->first()->update([
                'title'=>$request->title,
                'text'=>$request->text,
                'excerpt'=>$request->excerpt
            ]);
        }else{
            $changed=$item->translates()->create([
                'title'=>$request->title,
                'text'=>$request->text,
                'excerpt'=>$request->excerpt,
                'lang'=>$lang
            ]);
        }


        if(!$changed){
            DB::rollBack();
            return redirect()->back()->with('error',__('advertising::job.error'));
        }else{
            DB::commit();
            return redirect(route("werbungs.index"))->with('message',__('advertising::jobs.update'));
        }

    }
    public  function languageShowQuestion(Request $request,$lang,$token){
        $item=Question::with('translates')->where('token',$token)->first();
        return view('advertising::werbungs.question.language',compact('item','lang'));

    }
    public  function languageUpdateQuestion(Request $request,$lang,$token){

        DB::beginTransaction();
        $item=Question::with('translates')->where('token',$token)->first();
        if( $item->translates->where('lang',$lang)->first()){
            $changed=$item->translates->where('lang',$lang)->first()->update([
                'title'=>$request->title,
                'text'=>$request->text,
                'excerpt'=>$request->excerpt
            ]);
        }else{
            $changed=$item->translates()->create([
                'title'=>$request->title,
                'text'=>$request->text,
                'excerpt'=>$request->excerpt,
                'lang'=>$lang
            ]);
        }


        if(!$changed){
            DB::rollBack();
            return redirect()->back()->with('error',__('advertising::werbung.error'));
        }else{
            DB::commit();
            return redirect(route("werbungs.index"))->with('message',__('advertising::webung.update'));
        }

    }

    public function approved($token)
    {
        try {
            DB::beginTransaction();
           $setting=Setting::select('id', 'name', 'address','email','mobile','phone','fax','domain','slogan','copy_right')->with('info')->first();
            $item=Advertising::where('token',$token)->firstOrFail();
            $client=Client::find($item->client);
           

           

                $item->update([
                    'status'=>2
                ]);
                $result=[
                 
                    'email'=>$client->email,
                    'firstname'=>$client->first_name,
                    'setting'=>$setting,
                    'title'=>'Job Inserat',
    
                ];
    
                sendNotificationCustomEmail($result,'emails.front.approve-werbung'); 
            
                DB::commit();
                return redirect()->back()->with('message',__('client::clients.store'));
    

          

                DB::rollBack();
           
                return redirect()->back()->with('error',__('client::clients.error'));
  

        }catch (\Exception $exception){
            
            
            return dd($exception);
            DB::rollBack();
           
            return redirect()->back()->with('error',__('client::clients.error'));

        }
    }

   


}
