<?php
return [
    "text-create"=>"you can create your agent",
    "text-edit"=>"you can edit your agent",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"agents list",
    "singular"=>"agent",
    "collect"=>"agents",
    "permission"=>[
        "agent-full-access"=>"agents full access",
        "agent-list"=>"agents list",
        "agent-delete"=>"agent delete",
        "agent-create"=>"agent create",
        "agent-edit"=>"edit agent",
    ]
];
