<?php

namespace Modules\Widget\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Widget\Entities\Widget;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Widget::class)->delete();

        Permission::create(['name'=>'widget-list','model'=>Widget::class,'created_at'=>now()]);
        Permission::create(['name'=>'widget-create','model'=>Widget::class,'created_at'=>now()]);
        Permission::create(['name'=>'widget-edit','model'=>Widget::class,'created_at'=>now()]);
        Permission::create(['name'=>'widget-delete','model'=>Widget::class,'created_at'=>now()]);
    }
}
