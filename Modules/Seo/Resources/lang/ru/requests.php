<?php
return [
    "text-create"=>"you can create your request",
    "text-edit"=>"you can edit your request",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"requests list",
    "error"=>"error",
    "singular"=>"request",
    "collect"=>"requests",
    "permission"=>[
        "request-full-access"=>"request full access",
        "request-list"=>"request list",
        "request-delete"=>"request delete",
        "request-create"=>"request create",
        "request-edit"=>"edit request",
    ]
];
