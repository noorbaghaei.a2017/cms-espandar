<?php

namespace Modules\Comment\Entities\Repository;



interface CommentRepositoryInterface
{
    public function getAll();
}
