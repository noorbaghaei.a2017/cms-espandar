<?php
return [
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"customers listc",
    "singular"=>"pay",
    "collect"=>"pays",
    "permission"=>[
        "pay-full-access"=>"pays full access",
        "pay-list"=>"pays list",
        "pay-delete"=>"pay delete",
        "pay-create"=>"pay create",
        "pay-edit"=>"edit pay",
    ]
];
