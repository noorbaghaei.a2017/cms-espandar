<?php

namespace Modules\Payment\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Payment\Entities\Pay;
use Modules\Payment\Entities\Payment;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Payment::class)->delete();

        Permission::create(['name'=>'payment-list','model'=>Payment::class,'created_at'=>now()]);
        Permission::create(['name'=>'payment-create','model'=>Payment::class,'created_at'=>now()]);
        Permission::create(['name'=>'payment-edit','model'=>Payment::class,'created_at'=>now()]);
        Permission::create(['name'=>'payment-delete','model'=>Payment::class,'created_at'=>now()]);



        DB::table('permissions')->whereModel(Pay::class)->delete();

        Permission::create(['name'=>'pay-full-access','model'=>Pay::class,'created_at'=>now()]);
        Permission::create(['name'=>'pay-list','model'=>Pay::class,'created_at'=>now()]);
        Permission::create(['name'=>'pay-create','model'=>Pay::class,'created_at'=>now()]);
        Permission::create(['name'=>'pay-edit','model'=>Pay::class,'created_at'=>now()]);
        Permission::create(['name'=>'pay-delete','model'=>Pay::class,'created_at'=>now()]);


    }
}
