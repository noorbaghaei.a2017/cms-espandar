<?php

namespace Modules\Client\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterClientAsonkala extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile'=>'required|regex:/(09)[0-9]{9}/|unique:clients,mobile',
            'email'=>'required|email|unique:clients,email',
            'first_name'=>'required|max:20',
            'last_name'=>'required|max:20',
            'password'=>'required|min:8',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
