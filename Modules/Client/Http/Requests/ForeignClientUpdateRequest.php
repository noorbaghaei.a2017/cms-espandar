<?php

namespace Modules\Client\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\ForiegnPostalCode;
use Illuminate\Http\Request;

class ForeignClientUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
       
        if(isset($request->mobile)){
        return [
            'email'=>'required|unique:clients,email,'.$this->client.',id',
            'mobile'=>'required|unique:clients,mobile,'.$this->client.',id',
            'city'=>'required',
            'username'=>'unique:clients,username,'.$this->client.',id',
            'address'=>'required',
            'city'=>'required',
            'postal_code'=>['required','numeric','digits:5',new ForiegnPostalCode()],
            'image'=>'mimes:jpeg,png,jpg|max:2000',
        ];
            }
            elseif(isset($request->phone)){
                return [
                    'email'=>'required|unique:clients,email,'.$this->client.',id',
                    'phone'=>'required',
                    'city'=>'required',
                    'username'=>'unique:clients,username,'.$this->client.',id',
                    'address'=>'required',
                    'city'=>'required',
                    'postal_code'=>['required','numeric','digits:5',new ForiegnPostalCode()],
                    'image'=>'mimes:jpeg,png,jpg|max:2000',
                ];
            }
            else{
                return [
                    'email'=>'required|unique:clients,email,'.$this->client.',id',
                    'mobile'=>'required|unique:clients,mobile,'.$this->client.',id',
                    'city'=>'required',
                    'username'=>'unique:clients,username,'.$this->client.',id',
                    'address'=>'required',
                    'city'=>'required',
                    'postal_code'=>['required','numeric','digits:5',new ForiegnPostalCode()],
                    'image'=>'mimes:jpeg,png,jpg|max:2000',
                ];
            }
           
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
