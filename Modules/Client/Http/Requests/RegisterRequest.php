<?php

namespace Modules\Client\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    protected function prepareForValidation()
    {
        if ($this->has('mobile'))
            $this->merge(['mobile'=>checkZeroFirst($this->mobile)]);


    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'first_name'=>'required',
           'last_name'=>'required',
           'email'=>'unique:clients,email,'.$this->token.',token|email',
           'mobile'=>'required|regex:/(9)[0-9]{9}/|digits:10|numeric|unique:clients,mobile,'.$this->token.',token',
           'code'=>'nullable|exists:codes,code',
           'password'=>'required|min:8'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
