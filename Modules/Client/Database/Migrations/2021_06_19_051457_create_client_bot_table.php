<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientBotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_bot', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client')->unsigned();;
            $table->foreign('client')->references('id')->on('clients')->onDelete('cascade');
            $table->string('count')->default(1);
            $table->text('description')->nullable();
            $table->string('title')->nullable();
            $table->morphs('botable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_bot');
    }
}
