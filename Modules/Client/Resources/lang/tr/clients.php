<?php
return [
    "text-create"=>"با استفاده از فرم زیر شما میتوانید کاربر جدید اضافه گنید",
    "text-edit"=>"با استفاده از فرم زیر شما میتوانید کاربر را ویرایش گنید",
    "store"=>"Başarıyla kaydedildi",
    "error"=>"Bilgileri kaydetmeyle ilgili sorun",
    "error-password"=>"error password",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"members list",
    "singular"=>"client",
    "collect"=>"clients",
    "permission"=>[
        "client-full-access"=>"client full access",
        "client-list"=>"client list",
        "client-delete"=>"client delete",
        "client-create"=>"client create",
        "client-edit"=>"edit client",
    ]
];
