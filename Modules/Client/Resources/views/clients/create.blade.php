
@extends('core::dashboard.main')

@section('content')
   
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('cms.create_client')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('jobs.index')}}">{{__('cms.bussiness')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.create_client')}}</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- /.container-fluid -->


  <!-- Main content -->
  <section class="content">


    <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{__('cms.create_client')}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{route('clients.store')}}" enctype="multipart/form-data">
                  @csrf
                <div class="card-body">
                @include('core::layout.alert-danger')
                <br>
<div class="row">
                  <div class="col-lg-12 col-md-12">
                  <div class="form-group">
                      
                      <input name="image" type="file" class="custom-file-input" id="customFile">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>

</div>

</div>
<br>

                 <div class="row">


                    <div class="col-lg-6 col-md-12"> 
                        <div class="form-group">
                            <label for="exampleInputName">{{__('cms.first_name')}}</label>
                            <input type="text" name="first_name" value="{{old('first_name')}}" class="form-control" id="exampleInputName" placeholder="{{__('cms.first_name')}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12"> 
                        <div class="form-group">
                            <label for="last_name">{{__('cms.last_name')}}</label>
                            <input type="text" name="last_name" value="{{old('last_name')}}" class="form-control" id="last_name" placeholder="{{__('cms.last_name')}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12"> 
                        <div class="form-group">
                            <label for="email">{{__('cms.email')}}</label>
                            <input type="text" name="email" value="{{old('email')}}" class="form-control" id="email" placeholder="{{__('cms.email')}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12"> 
                        <div class="form-group">
                            <label for="password">{{__('cms.password')}}</label>
                            <input type="text" name="password" value="{{old('password')}}" class="form-control" id="password" placeholder="{{__('cms.password')}}">
                        </div>
                    </div>

               
                </div>
  

</div>
                  
                  
                
                 
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">{{__('cms.save')}}</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->

    
    </section>
    <!-- /.content -->
 
  
   


    


@endsection


@section('scripts')

<script>




</script>


@endsection












