<li>
        <a href="{{route('seekers.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('client.icons.seeker')}}"></i>
                              </span>
            <span class="nav-text">{{__('client::seekers.collect')}}</span>
        </a>
</li>
