<?php


namespace Modules\Information\Entities\Repository;


interface InformationRepositoryInterface
{
    public function getAll();

}
