<?php

namespace Modules\Information\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Information\Entities\Information;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();


        DB::table('permissions')->whereModel(Information::class)->delete();

        Permission::create(['name'=>'information-list','model'=>Information::class,'created_at'=>now()]);
        Permission::create(['name'=>'information-create','model'=>Information::class,'created_at'=>now()]);
        Permission::create(['name'=>'information-edit','model'=>Information::class,'created_at'=>now()]);
        Permission::create(['name'=>'information-delete','model'=>Information::class,'created_at'=>now()]);


    }
}
