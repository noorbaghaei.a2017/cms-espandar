<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'),"middleware"=>["auth:web"]],function() {
    Route::resource('/plans', 'PlanController')->only('create','store','destroy','update','index','edit');

    Route::group(["prefix"=>'search', "middleware" => ["auth:web"]], function () {
        Route::post('/plans', 'PlanController@search')->name('search.plan');
    });

    Route::group(["prefix"=>'plan/categories'], function () {
        Route::get('/', 'PlanController@categories')->name('plan.categories');
        Route::get('/create', 'PlanController@categoryCreate')->name('plan.category.create');
        Route::post('/store', 'PlanController@categoryStore')->name('plan.category.store');
        Route::get('/edit/{category}', 'PlanController@categoryEdit')->name('plan.category.edit');
        Route::patch('/update/{category}', 'PlanController@categoryUpdate')->name('plan.category.update');

    });

    Route::group(["prefix"=>'plan/questions'], function () {
        Route::get('/{plan}', 'PlanController@question')->name('plan.questions');
        Route::get('/create/{plan}', 'PlanController@questionCreate')->name('plan.question.create');
        Route::post('/store/{plan}', 'PlanController@questionStore')->name('plan.question.store');
        Route::delete('/destroy/{question}', 'PlanController@questionDestroy')->name('plan.question.destroy');
        Route::get('/edit/{plan}/{question}', 'PlanController@questionEdit')->name('plan.question.edit');
        Route::patch('/update/{question}', 'PlanController@questionUpdate')->name('plan.question.update');
    });

});
