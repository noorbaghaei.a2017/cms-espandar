<?php

namespace Modules\Member\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Information\Entities\Information;
use Modules\Member\Entities\Member;
use Modules\Member\Entities\MemberRole;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Member::class)->delete();


        Permission::create(['name'=>'member-list','model'=>Member::class,'created_at'=>now()]);
        Permission::create(['name'=>'member-create','model'=>Member::class,'created_at'=>now()]);
        Permission::create(['name'=>'member-edit','model'=>Member::class,'created_at'=>now()]);
        Permission::create(['name'=>'member-delete','model'=>Member::class,'created_at'=>now()]);

        Permission::create(['name'=>'side-list','model'=>MemberRole::class,'created_at'=>now()]);
        Permission::create(['name'=>'side-create','model'=>MemberRole::class,'created_at'=>now()]);
        Permission::create(['name'=>'side-edit','model'=>MemberRole::class,'created_at'=>now()]);
    }
}
