<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('users')->delete();


        $users=[
            [
                'first_name'=>"amin",
                'last_name'=>"nourbaghaei",
                'username'=>"nourbaghaei",
                'direction'=>"ltr",
                'lang'=>"de",
                'type'=>"direct-iniaz",
                'name'=>"amin",
                'mobile'=>"9195995044",
                'country' => '+98',
                'token'=>Str::random(),
                "email"=>"noorbaghaei.a2017@gmail.com",
                "password"=>Hash::make('secret_nourbaghaei'),
                "created_at"=>now()
            ],
           
          
            
        ];


        DB::table('users')->insert($users);

    }
}
