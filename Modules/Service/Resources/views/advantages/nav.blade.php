
    <li>
        <a href="{{route('advantages.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('service.icons.advantage')}}"></i>
                              </span>
            <span class="nav-text"> {{__('service::advantages.collect')}}</span>
        </a>
    </li>
