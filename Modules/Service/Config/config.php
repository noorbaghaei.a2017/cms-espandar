<?php

return [
    'name' => 'Service',
    'icons'=>[
        'advantage'=>'ion-ios-compose',
        'property'=>'ion-ios-compose',
        'service'=>'fa fa-crosshairs',
    ]
];
