<?php


namespace Modules\Event\Entities\Repository;


interface EventRepositoryInterface
{

    public function getAll();
}
