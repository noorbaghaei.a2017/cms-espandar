<?php
return [
    "text-create"=>"you can create your event",
    "text-edit"=>"you can edit your event",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"events list",
    "error"=>"error",
    "singular"=>"event",
    "collect"=>"events",
    "permission"=>[
        "event-full-access"=>"event full access",
        "event-list"=>"events list",
        "event-delete"=>"event delete",
        "event-create"=>"event create",
        "event-edit"=>"edit event",
    ]


];
