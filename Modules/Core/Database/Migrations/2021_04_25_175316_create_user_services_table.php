<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('service')->unsigned()->index();
            $table->foreign('service')->references('id')->on('list_services')->onDelete('cascade');
            $table->bigInteger('user')->unsigned()->nullable();;
            $table->foreign('user')->references('id')->on('clients')->onDelete('cascade');
            $table->text('excerpt')->nullable();
            $table->string('latitude')->nullable();
            $table->string('address_two')->nullable();
            $table->string('longitude')->nullable();
            $table->boolean('banner_status')->default(1);
            $table->text('text')->nullable();
            $table->string('title');
            $table->string('postal_code');
            $table->string('slug');
            $table->tinyInteger('is_special')->default(0);
            $table->string('status')->index()->default(0);
            $table->string('country');
            $table->string('city')->index();
            $table->string('refer_code')->nullable();
            $table->text('google_map');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone_2')->nullable();
            $table->string('mobile')->nullable();
            $table->text('address')->nullable();
            $table->string('token')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_services');
    }
}
