<?php


namespace Modules\Core\Helper;


use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\Product;

class CategoryHelper
{


    public static function status($status){
        switch ($status){
            case 1 :
                return '<span class="alert-success">فعال</span>';
                break;
            case 0 :
                return '<span class="alert-warning">غیر فعال</span>';
                break;
            default:
                return '<span class="alert-danger">خطای سیستمی</span>';
                break;

        }
    }

}
