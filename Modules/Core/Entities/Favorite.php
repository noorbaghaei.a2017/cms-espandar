<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $table="favorites";
    protected $fillable = ['title','text','excerpt','client'];

    public function favoriteable()
    {
        return $this->morphTo();
    }
}
