<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class AnalyticsClient extends Model
{
    protected $fillable = ['ip','analytic','device','latitude','longitude'];

    protected $table='analytics_client';

}
