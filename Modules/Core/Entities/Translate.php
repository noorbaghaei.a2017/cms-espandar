<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Translate extends Model
{
    protected $fillable = ['lang','title','answer','slug','text','excerpt','symbol','name','copy_right','address','slogan','title_seo','canonical_seo','description_seo','keyword_seo'];

    public function translateable()
    {
        return $this->morphTo();
    }
}
