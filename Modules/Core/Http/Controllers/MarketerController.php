<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Entities\User;
use Modules\Core\Http\Requests\MarketerStoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class MarketerController extends Controller
{
    public function __construct()
    {
        $this->entity=new User();

    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->where('type','marketer-iniaz')->paginate(20);
            return view('core::dashboard.marketers.index',compact('items'));
        }catch (\Exception $exception){
            
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('core::dashboard.marketers.create');
        }catch (\Exception $exception){
          
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(MarketerStoreRequest $request)
    {
        try {

            $this->entity->first_name=$request->input('first_name');
            $this->entity->last_name=$request->input('last_name');
            $this->entity->type='marketer-iniaz';
            $this->entity->mobile=$request->input('mobile');
            $this->entity->direction='ltr';
            $this->entity->country='german';
            $this->entity->lang='de';
            $this->entity->email=$request->input('email');
            $this->entity->password=Hash::make($request->password);
            $this->entity->token=tokenGenerate();
            $this->entity->save();


            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$this->entity->save()){
                return redirect()->back()->with('error',__('client::clients.error'));
            }
            else{
                return redirect(route("marketers.index"))->with('message',__('client::clients.store'));
            }


        }catch (\Exception $exception){
          
            return redirect()->back()->with('error',__('client::clients.error'));

        }
    }

  

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            return view('core::dashboard.marketers.edit',compact('item'));
        }catch (\Exception $exception){
          
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $token)
    {
        try {

            $validator=Validator::make($request->all(),[
                'mobile'=>'required',
                'email'=>'required|unique:users,email,'.$token.',token'
            ]);
            if($validator->fails()){
                return  redirect()->back()->withErrors($validator);
            }


            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                "first_name"=>$request->input('first_name'),
                "last_name"=>$request->input('last_name'),
                "mobile"=>$request->input('mobile'),
                "email"=>$request->input('email'),

            ]);


            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }



            if(!$updated){
                return redirect()->back()->with('error',__('client::clients.error-password'));
            }else{
                return redirect(route("marketers.index"))->with('message',__('client::clients.update'));

            }
        }catch (\Exception $exception){
           
            return redirect()->back()->with('error',__('client::clients.error-password'));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
