<?php


namespace Modules\Core\Http\Controllers;


use Illuminate\Http\Request;
use Modules\Core\Entities\Category;
use Modules\Core\Http\Requests\CategoryRequest;

trait HasCategory
{
    public  function categories(){

        try {
            $categoriesproductcount=Category::latest()->where('model',$this->class)->count();  
            $facategoriesproductcount=Category::latest()->where('model',$this->class)->whereHas('translates',function ($query){
                $query->where('lang', '=', 'fa');
            })
            ->count();
            $encategoriesproductcount= Category::latest()->where('model',$this->class)->whereHas('translates',function ($query){
                $query->where('lang', '=' ,'en');
            })
            ->count();
            $origin=Category::with('translates')->latest()->where('model',$this->class)->where('parent',0)->get();
            $sub=Category::with('translates')->latest()->where('model',$this->class)->where('parent','<>',0)->get();

            return view($this->route_categories_index,compact('origin','sub','categoriesproductcount','facategoriesproductcount','encategoriesproductcount'));
        }catch (\Exception $exception){
         
            return abort('500');
        }
    }

    public  function categoryCreate(){
        try {
            $parent_categories=Category::with('translates')->latest()->where('parent',0)->where('model',$this->class)->get();
            return view($this->route_categories_create,compact('parent_categories'));

        }catch (\Exception $exception){
            return redirect()->back()->with('error',__('core::categories.error'));

        }
    }

    public  function categoryStore(CategoryRequest $request){
        try {
            $parent=-1;
            if($request->input('parent')!=-1){
                $parent=Category::whereToken($request->input('parent'))->first();
                $level=intval($parent->level )+1;
            }
            $saved=Category::create([
                'user'=>auth('web')->user()->id,
                'title'=>$request->input('title'),
                'icon'=>$request->input('icon'),
                'status'=>$request->input('status'),
                'excerpt'=>$request->input('excerpt'),
                'model'=>$this->class,
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
                'order'=>orderInfo($request->input('order')),
                'token'=>tokenGenerate(),
            ]);

            $saved->analyzer()->create();

            if($request->has('image')){
                $saved->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$saved){
                return redirect()->back()->with('error',__('core::categories.error'));
            }else{
                return redirect(route($this->route_categories))->with('message',__('core::categories.store'));
            }

        }catch (\Exception $exception){
           
            return redirect()->back()->with('error',__('core::categories.error'));
        }
    }

    public  function CategoryEdit(Request $request,$token){
        try {

            $item=Category::whereToken($token)->first();
            $parent_categories=Category::latest()->where('model',$this->class)->where('parent',0)->where('token','!=',$token)->get();

            return view($this->route_categories_edit,compact('item','parent_categories'));

        }catch (\Exception $exception){
          
            return redirect()->back()->with('error',__('core::categories.error'));

        }
    }
    public  function categoryUpdate(CategoryRequest $request,$category){
        try {
           
            $parent=-1;
            
            if($request->input('parent')!=-1){
                $parent=Category::whereToken($request->input('parent'))->first();
                
            }

            $item=Category::whereToken($category)->first();

            $updated=$item->update([
                'title'=>$request->input('title'),
                "slug"=>null,
                'status'=>$request->input('status'),
                'order'=>orderInfo($request->input('order')),
                'icon'=>$request->input('icon'),
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
            ]);

            if($request->has('image')){
                destroyMedia($item,config('cms.collection-image'));
                $item->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$updated){
                return redirect()->back()->with('error',__($this->notification_error));
            }else{
                return redirect(route($this->route_categories))->with('message',__('core::categories.store'));
            }

        }catch (\Exception $exception){
           
            return redirect()->back()->with('error',__('core::categories.error'));

        }
    }

}
