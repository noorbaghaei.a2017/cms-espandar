<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Core\Entities\Country;
use Modules\Core\Entities\Setting;
use Modules\Core\Entities\Template;

class SettingController extends Controller
{

    protected $entity;
    protected $template;

    public function __construct()
    {
        $this->entity=new Setting();
        $this->template=new Template();

    }
    public  function setting(){
        try {
             $item=$this->entity->latest()->firstOrFail();
            return view('core::layout.setting.setting',compact('item'));
        }catch (\Exception $exception){
         
            return redirect()->back()->with('error',__('core::settings.error'));

        }
    }
    public  function footer(){
        try {
            $item=$this->entity->latest()->firstOrFail();
            return view('core::layout.setting.footer',compact('item'));
        }catch (\Exception $exception){
           
            return redirect()->back()->with('error',__('core::settings.error'));

        }
    }
    public  function template(){
        try {
            $items=$this->template->whereStatus(1)->get();
            return view('core::layout.setting.template',compact('items'));
        }catch (\Exception $exception){
          
            return redirect()->back()->with('error',__('core::settings.error'));

        }
    }
    public function settingUpdate(Request $request){
            try {
              
                $this->entity=$this->entity->firstOrFail();

                $this->entity->update([
                    'name'=>$request->input('name'),
                    'domain'=>$request->input('domain'),
                    'excerpt'=>$request->input('excerpt'),
                    'email'=>$request->input('email'),
                    'slogan'=>$request->input('slogan'),
                    'ios_app'=>$request->input('ios_app'),
                    'android_app'=>$request->input('android_app'),
                    'google_map'=>$request->input('map'),
                    'copy_right'=>$request->input('copy'),
                    'address'=>$request->input('address'),
                    'mobile'=>$request->input('mobile'),
                    'phone'=>$request->input('phone'),
                    'fax'=>$request->input('fax'),
                ]);

                $this->entity->seo->update([
                    'title'=>$request->title_seo,
                    'description'=>$request->description_seo
                ]);

                if($request->has('image')){
                    destroyMedia($this->entity,'logo');
                    $this->entity->addMedia($request->file('image'))->toMediaCollection('logo');
                }
               
                return redirect()->back()->with('message',__('core::settings.update'));

            }catch (\Exception $exception){
                
            return dd($exception);
                return redirect()->back()->with('error',__('core::settings.error'));

            }


    }
    public function footerUpdate(Request $request){

            try {

                $this->entity=$this->entity->firstOrFail();

                if($request->has('image')){
                    destroyMedia($this->entity,'footer');
                    $this->entity->addMedia($request->file('image'))->toMediaCollection('footer');
                }

                return redirect()->back()->with('message',__('core::settings.update'));

            }catch (\Exception $exception){
                return redirect()->back()->with('error',__('core::settings.error'));

            }


    }


    public  function languageShow(Request $request,$lang,$id){
        $item=Setting::with('translates')->find($id);
        return view('core::settings.language',compact('item','lang'));

    }
    public  function languageUpdate(Request $request,$lang,$id){

        DB::beginTransaction();
        $item=Setting::with('translates')->find($id);
        if( $item->translates->where('lang',$lang)->first()){
            $changed=$item->translates->where('lang',$lang)->first()->update([
                'copy_right'=>$request->copy_right,
                'address'=>$request->address,
                'name'=>$request->name,
                'slogan'=>$request->slogan,
                'excerpt'=>$request->excerpt,
                'description_seo'=>$request->description_seo,
                'title_seo'=>$request->title_seo,
                'keyword_seo'=>$request->keyword_seo,
                'canonical_seo'=>$request->canonical_seo,
            ]);
        }else{
            $changed=$item->translates()->create([
                'copy_right'=>$request->copy_right,
                'address'=>$request->address,
                'slogan'=>$request->slogan,
                'excerpt'=>$request->excerpt,
                'name'=>$request->name,
                'description_seo'=>$request->description_seo,
                'title_seo'=>$request->title_seo,
                'keyword_seo'=>$request->keyword_seo,
                'canonical_seo'=>$request->canonical_seo,
                'lang'=>$lang
            ]);
        }


        if(!$changed){
            DB::rollBack();
            return redirect()->back()->with('error',__('core::settings.error'));
        }else{
            DB::commit();
            return redirect()->back()->with('message',__('core::settings.update'));        }

    }
}


