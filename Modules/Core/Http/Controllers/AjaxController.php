<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Chat\Entities\Chat;
use Modules\Client\Entities\Client;
use Modules\Core\Entities\Setting;
use Modules\Core\Entities\User;

class AjaxController extends Controller
{
    public function getChats(Request $request,$client){

        $items=Chat::where('client',$client)->get();
        $setting=Setting::with('info')->first();

        $result=[];
        $client=Client::with('info')->find($client);

        $information=[
            'full_name'=>$client->full_name,
            'image'=>!$client->Hasmedia('images') ? asset('img/no-img.gif') : asset($client->getFirstMediaUrl('images')) ,
            'mobile'=>$client->mobile,
            'email'=>$client->email,
            'about'=>$client->info->about,
        ];

        foreach ($items as $item) {

                $result[]=[
                    'id'=>$item->id,
                    'client'=>$item->client,
                    'full_name'=>Client::find($item->client)->full_name,
                    'clientImage'=>!Client::find($item->client)->Hasmedia('images') ? asset('img/no-img.gif') : asset(Client::find($item->client)->getFirstMediaUrl('images')) ,
                    'userImage'=>!$setting->Hasmedia('logo') ? asset('img/no-img.gif') : asset($setting->getFirstMediaUrl('logo')) ,
                    'text'=>$item->text,
                    'user'=>$item->user,
                    'created_at'=>$item->created_at->ago(),
                    'email'=>Client::find($item->client)->email,
                    'mobile'=>Client::find($item->client)->mobile,
                ];

                if($item->user==null){
                    $item->update([
                        'status'=>2
                    ]);
                }


        }

        return response()->json(['result'=>$result,'info'=>$information],200);

    }
    public function sendMessage(Request $request,$client,$message){


        Chat::create([
            'client'=>Client::find($client)->id,
            'user'=>auth('web')->user()->id,
            'text'=>$message,
            'created_at'=>now(),
        ]);

        $items=Chat::where('client',$client)->get();
        $setting=Setting::with('info')->first();

        $client=Client::with('info')->find($client);

        $information=[
            'full_name'=>$client->full_name,
            'image'=>!$client->Hasmedia('images') ? asset('img/no-img.gif') : asset($client->getFirstMediaUrl('images')) ,
            'mobile'=>$client->mobile,
            'email'=>$client->email,
            'about'=>$client->info->about,
        ];

        $result=[];
        foreach ($items as $item) {

                $result[]=[
                    'id'=>$item->id,
                    'client'=>$client,
                    'full_name'=>Client::find($item->client)->full_name,
                    'clientImage'=>!Client::find($item->client)->Hasmedia('images') ? asset('img/no-img.gif') : asset(Client::find($item->client)->getFirstMediaUrl('images')) ,
                    'userImage'=>!$setting->Hasmedia('logo') ? asset('img/no-img.gif') : asset($setting->getFirstMediaUrl('logo')) ,
                    'text'=>$item->text,
                    'user'=>$item->user,
                    'created_at'=>$item->created_at->ago(),
                    'email'=>Client::find($item->client)->email,
                    'mobile'=>Client::find($item->client)->mobile,
                ];


        }

        return response()->json(['result'=>$result,'info'=>$information],200);

    }
}
