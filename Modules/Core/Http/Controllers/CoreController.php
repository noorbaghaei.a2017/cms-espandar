<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\User;


class CoreController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            return view('core::dashboard.index');
        }catch (\Exception $exception){

        }
    }

    public function contactsAdmin(){
        try {
            $admins=User::latest()->where('type','direct-iniaz')->get();
            return view('core::dashboard.contacts',compact('admins'));
        }catch (\Exception $exception){

        }
    }
    public function contactsMarketer(){
        try {
          
            $marketers=User::latest()->where('type','marketer-iniaz')->get();
           
            return view('core::dashboard.marketers',compact('marketers'));
        }catch (\Exception $exception){
return dd($exception);
        }
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function meeting()
    {
        try {
            return view('core::layout.meeting');
        }catch (\Exception $exception){

        }
    }
    public function myDesk()
    {
        try {
            return view('core::member.dashboard');
        }catch (\Exception $exception){

        }
    }
    public function login()
    {
        try {
            return view('core::member.auth.login');
        }catch (\Exception $exception){

        }
    }

    public function changeLangAdmin($lang){

       try{
        $admin=auth('web')->user();
     

        if($lang!='fa' && $lang!='ar'){
            $admin->update([
                'lang'=>$lang,
                'direction'=>'ltr'
            ]);
        }
        else{
            $admin->update([
                'lang'=>$lang,
                'direction'=>'rtl'
            ]); 
        }

        return redirect(route('dashboard.website'));

       }catch(\Exception $exception){
        return abort('500');
       }
    }



}
