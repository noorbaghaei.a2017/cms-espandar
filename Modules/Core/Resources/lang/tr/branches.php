<?php
return [
    "text-create"=>"you can create your branche",
    "text-edit"=>"you can edit your branche",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "error"=>"error",
    "index"=>"branches list",
    "singular"=>"branche",
    "collect"=>"branches",
    "permission"=>[
        "branche-full-access"=>"branches full access",
        "branche-list"=>"branches list",
        "branche-delete"=>"branche delete",
        "branche-create"=>"branche create",
        "branche-edit"=>"edit branche",
    ]
];
