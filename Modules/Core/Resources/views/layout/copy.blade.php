<div class="app-footer white bg p-a b-t">
    <div class="pull-right text-sm text-muted">{{__('cms.version')}} {{config('cms.version')}}</div>
    @if(env('SET_CUSTOMER'))
        <span class="text-sm text-muted">{{__('cms.copy_customer')}}</span>
    @else
        <span class="text-sm text-muted">{{__('cms.copy')}}</span>

    @endif

</div>
