
@extends('core::dashboard.main')

@section('content')
   
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('cms.setting')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.setting')}}</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- /.container-fluid -->


  <!-- Main content -->
  <section class="content">
      <div class="row">
      


      <div class="col-12 col-sm-4">
              <h3 class="d-inline-block d-sm-none">LOWA Men’s Renegade GTX Mid Hiking Boots Review</h3>
              <div class="col-12">
              @if(!$setting->Hasmedia('logo'))
              <img src="{{asset('img/no-img.gif')}}" class="product-image" alt="">
       
    @else
    <img src="{{$setting->getFirstMediaUrl('logo')}}" class="product-image" alt="">
      
    @endif
              
              </div>
             
            </div>
            <div class="col-12 col-sm-8">
              <h3 class="my-3">{{$setting->name}}</h3>
              <p class="my-3">{{$setting->slogan}}</p>
             
            
            </div>





    </div>


    <br>
    <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{__('cms.setting')}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{route('dashboard.setting.update')}}" method="POST" enctype="multipart/form-data">
              {{csrf_field()}}
                                    {{method_field('PATCH')}}
                <div class="card-body">
                @include('core::layout.alert-danger')
              @include('core::layout.alert-success')
                <div class="form-group">
                        <label for="image" class="col-sm-2 col-form-label">{{__('image')}}</label>
                        <div class="col-sm-10">
                          <input type="file" name="image"  class="form-control" id="image" >
                        </div>
                      </div>

                <div class="form-group">
                    <label for="exampleInputName">{{__('cms.title')}}</label>
                    <input type="text" name="name" value="{{$setting->name}}" class="form-control" id="exampleInputName" placeholder="{{__('cms.title')}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputSlogan">{{__('cms.slogan')}}</label>
                    <input type="text" name="slogan" value="{{$setting->slogan}}" class="form-control" id="exampleInputSlogan" placeholder="{{__('cms.slogan')}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputDomain">{{__('cms.domain')}}</label>
                    <input type="text" name="domain" value="{{$setting->domain}}" class="form-control" id="exampleInputDomain" placeholder="{{__('cms.domain')}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputIos">{{__('cms.ios_app')}}</label>
                    <input type="text" name="ios_app" value="{{$setting->ios_app}}" class="form-control" id="exampleInputIos" placeholder="{{__('cms.ios_app')}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputAndroid">{{__('cms.ios_app')}}</label>
                    <input type="text" name="android_app" value="{{$setting->android_app}}" class="form-control" id="exampleInputAndroid" placeholder="{{__('cms.android_app')}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputMobile">{{__('cms.mobile')}}</label>
                    <input type="text" name="mobiles" value="{{$setting->mobile}}" class="form-control" id="exampleInputMobile" placeholder="{{__('cms.mobile')}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFax">{{__('cms.fax')}}</label>
                    <input type="text" name="faxs" value="{{$setting->fax}}" class="form-control" id="exampleInputFax" placeholder="{{__('cms.fax')}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">{{__('cms.phone')}}</label>
                    <input type="text" name="phones" value="{{$setting->phone}}" class="form-control" id="exampleInputPhone" placeholder="{{__('cms.phone')}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputName">{{__('cms.address')}}</label>
                    <input type="text" name="address" value="{{$setting->address}}" class="form-control" id="exampleInputName" placeholder="{{__('cms.address')}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputMap">{{__('cms.map')}}</label>
                    <input type="text" name="google_map" value="{{$setting->google_map}}" class="form-control" id="exampleInputMap" placeholder="{{__('cms.map')}}">
                  </div>


                  
                  
                  <div class="form-group">
                    <label for="exampleInputEmail1">{{__('cms.email')}}</label>
                    <input type="email" value="{{$setting->email}}" name="email" class="form-control" id="exampleInputEmail1" placeholder="{{__('cms.email')}}">
                  </div>

                 
                 
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">{{__('cms.save')}}</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->

    
    </section>
    <!-- /.content -->
 
  
   


    


@endsection












