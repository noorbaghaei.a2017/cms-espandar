<a title="{{$private->title}}" class="btn btn-primary btn-sm text-sm text-white" data-toggle="modal" data-target="#detail-{{$values->token}}" data-ui-toggle-class="rotate" data-ui-target="#detail"><span class="{{$private->icon}}"></span></a>

<div id="detail-{{$values->token}}" class="modal fade animate" data-backdrop="true">
    <div class="modal-dialog" id="detail">
        <div class="modal-content">

            <div class="modal-body text-center p-lg">



            </div>
            <div class="modal-footer">


                <button class="btn btn-danger p-x-md" data-dismiss="modal">{{__('cms.ok')}}</button>
            </div>
        </div>
    </div>
</div>
