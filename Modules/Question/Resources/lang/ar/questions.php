<?php
return [
    "text-create"=>"you can create your questions",
    "text-edit"=>"you can edit your questions",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"questions list",
    "singular"=>"questions",
    "collect"=>"questions",
    "permission"=>[
        "questions-full-access"=>"questions full access",
        "questions-list"=>"questions list",
        "questions-delete"=>"questions delete",
        "questions-create"=>"questions create",
        "questions-edit"=>"edit questions",
    ]
];
