<?php


namespace Modules\Product\Entities\Repository\Product;


interface ProductRepositoryInterface
{

    public function getAll();
}
