<?php

namespace Modules\Product\Transformers\OrderList;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderListProductCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $item ) {
                    return new OrderListProductResource($item);
                }
            ),
            'filed' => [
                'order_id','title','full_name','mobile','email','price','status','update_date','create_date'
            ],
            'public_route'=>[

            ],
            'private_route'=>
                [

                ],
            'search_route'=>[

                'name'=>'search.product.orders',
                'filter'=>[
                    'title','order_id'
                ],
                'icon'=>config('cms.icon.add'),
                'title'=>__('cms.add'),
                'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                'method'=>'POST',

            ],
            'count' => $this->collection->count(),
            'links' => [
                'self' => 'link-value',
            ],
            'collect'=>__('order::orders.collect'),
            'title'=>__('order::orders.index'),
        ];
    }
}
