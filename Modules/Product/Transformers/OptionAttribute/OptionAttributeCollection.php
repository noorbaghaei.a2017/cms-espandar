<?php

namespace Modules\Product\Transformers\OptionAttribute;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Product\Entities\OptionAttribute;

class OptionAttributeCollection extends ResourceCollection
{
    public $collects = OptionAttribute::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $item ) {
                    return new OptionAttributeResource($item);
                }
            ),
            'filed' => [
                'title','slug','order','update_date','create_date',
            ],
            'public_route'=>[

                [
                    'name'=>'product.create.attribute',
                    'param'=>[null],
                    'icon'=>config('cms.icon.add'),
                    'title'=>__('cms.add'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                    'method'=>'GET',
                ]

            ],
            'private_route'=>
                [
                    [
                        'name'=>'product.edit.attribute',
                        'param'=>[
                            'attribute'=>'token'
                        ],
                        'icon'=>config('cms.icon.edit'),
                        'title'=>__('cms.edit'),
                        'class'=>'btn btn-warning btn-sm text-sm',
                        'modal'=>false,
                        'method'=>'GET',
                    ],
                    [
                        'name'=>'attributes.options',
                        'param'=>[
                            'token'=>'token'
                        ],
                        'icon'=>'fa fa-bars',
                        'title'=>__('cms.options'),
                        'class'=>'btn btn-warning btn-sm text-sm text-white',
                        'modal'=>false,
                        'method'=>'GET',
                    ]

                ],
            'search_route'=>[

                'name'=>'search.product.attribute',
                'filter'=>[
                    'title','slug','order'
                ],
                'icon'=>config('cms.icon.add'),
                'title'=>__('cms.add'),
                'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                'method'=>'POST',

            ],
            'count' => $this->collection->count(),
            'links' => [
                'self' => 'link-value',
            ],
            'collect'=>__('product::attributes.collect'),
            'title'=>__('product::attributes.index'),
        ];
    }
}
