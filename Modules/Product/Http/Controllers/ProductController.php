<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Client\Entities\Client;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Currency;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasGallery;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Order\Entities\Order;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\OptionAttribute;
use Modules\Product\Entities\OptionProperty;
use Modules\Product\Entities\Repository\Product\ProductRepositoryInterface;
use Modules\Product\Http\Requests\ProductRequest;
use Modules\Product\Transformers\OrderList\OrderListProductCollection;
use Modules\Product\Transformers\Product\ProductCollection;
use Modules\Store\Entities\Store;

class ProductController extends Controller
{
    use HasQuestion,HasCategory,HasGallery;

    protected $entity;

    protected $class;

    private  $repository;

//category

    protected $route_categories_index='product::categories.index';
    protected $route_categories_create='product::categories.create';
    protected $route_categories_edit='product::categories.edit';
    protected $route_categories='product.categories';

//question

    protected $route_questions_index='product::questions.index';
    protected $route_questions_create='product::questions.create';
    protected $route_questions_edit='product::questions.edit';
    protected $route_questions='products.index';


//gallery

    protected $route_gallery_index='product::products.gallery';
    protected $route_gallery='products.index';



//notification

    protected $notification_store='product::products.store';
    protected $notification_update='product::products.update';
    protected $notification_delete='product::products.delete';
    protected $notification_error='product::products.error';



    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->entity=new Product();

        $this->class=Product::class;

        $this->repository=$repository;

        $this->middleware('permission:product-list')->only('index');
        $this->middleware('permission:product-create')->only(['create','store']);
        $this->middleware('permission:product-edit' )->only(['edit','update']);
        $this->middleware('permission:product-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
       

        try {

            $items=Product::with('user_info')->latest()->get();

            return view('product::products.index',compact('items'));

        }catch (\Exception $exception){
           
            return abort('500');
        }

    }
     /**
     * Display a listing of the resource.
     * @return Response
     */
    public function reserve()
    {
       

        try {

            $items=Product::with('user_info')->where('user',null)->latest()->get();

            return view('product::products.index',compact('items'));

        }catch (\Exception $exception){
           
           
            return abort('500');
        }

    }
      /**
     * Display a listing of the resource.
     * @return Response
     */
    public function new()
    {
       

        try {

            $items=Product::with('user_info')->where('status',1)->latest()->get();

            return view('product::products.index',compact('items'));

        }catch (\Exception $exception){
           
            return abort('500');
        }

    }

    /**
     * Display a listing of the resource.
     * @param $token
     * @return Response
     */
    public function orders($token)
    {
        try {

            $items=Order::with('payment')->get();

            $result = new OrderListProductCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $clients=Client::latest()->get();
            $option_properties=OptionProperty::with('options')->get();
            $option_attributes=OptionAttribute::with('options')->get();
            $parent_products=$this->entity->latest()->whereParent(0)->get();
            $categories=Category::latest()->where('model',Product::class)->where('parent','<>',0)->get();
            $stores=Category::latest()->where('model',Store::class)->get();
            $currencies=Currency::latest()->get();
            return view('product::products.create',compact('categories','stores','parent_products','currencies','option_properties','option_attributes','clients'));
        }catch (\Exception $exception){
           
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {

            if(
                !isset($request->title) &&
                !isset($request->code)
            ){
                $items=$this->repository->getAll();

                $result = new ProductCollection($items);

                $data= collect($result->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }

            $items=$this->entity
                ->where("title",trim($request->title))
                ->orwhere("code",trim($request->code))
                ->paginate(config('cms.paginate'));

            $result = new ProductCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param ProductRequest $request
     * @return Response
     */
    public function store(ProductRequest $request)
    {
        try {


            DB::beginTransaction();
          

            $this->entity->user=$request->client=='0' ? null : Client::whereToken($request->input('client'))->first()->id;
            $this->entity->title=$request->input('title');
            $this->entity->code=codeProduct();
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->postal_code=$request->input('postal_code');
            $this->entity->city=$request->input('city');
            $this->entity->mobile=$request->input('mobile');
            $this->entity->address=$request->input('address');
            $this->entity->country='german';
            $this->entity->parent=0;
            $this->entity->status=2;
            $this->entity->text=$request->input('text');
            $this->entity->status=$request->input('status');
            $this->entity->special=$request->input('special');
            $this->entity->category=Category::whereToken($request->input('category'))->first()->id;
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            $this->entity->attachTags([]);

            if(!isNot($request->input('price'))){
                $this->entity->price()->create([
                    'amount'=>$request->input('price'),
                ]);
            }
            else{
                $this->entity->price()->create([
                    'amount'=>0,
                ]);
            }

            $this->entity->seo()->create([
               
            ]);

            $this->entity->discount()->create([
             
            ]);

            if($request->has('attributes')){
                $this->entity->syncAttribute([]);
            }

            if($request->has('properties')) {
                $this->entity->syncProperty([]);
            }



          

            $this->entity->analyzer()->create();

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$saved){
                DB::rollBack();
                return redirect()->back()->with('error',__('product::products.error'));
            }else{
                DB::commit();
                return redirect(route('products.index'))->with('message',__('product::products.store'));
            }


        }catch (\Exception $exception){
            DB::rollBack();
            return redirect()->back()->with('error',__('product::products.error'));

        }
    }




    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $clients=Client::latest()->get();
            $option_properties=OptionProperty::with('options')->get();
            $option_attributes=OptionAttribute::with('options')->get();
            $currencies=Currency::latest()->get();
            $parent_products=$this->entity->latest()->whereParent(0)->where('token','!=',$token)->get();
            $categories=Category::latest()->where('model',Product::class)->where('parent','<>',0)->get();
            $stores=Category::latest()->where('model',Store::class)->get();
            $item=$this->entity->with('attributes','properties')->whereToken($token)->first();
            return view('product::products.edit',compact('item','categories','stores','parent_products','currencies','option_attributes','option_properties','clients'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param ProductRequest $request
     * @param $token
     * @return void
     */
    public function update(ProductRequest $request, $token)
    {
        try {

            DB::beginTransaction();
            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }


            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                "user"=>$request->client=='0' ? null : Client::whereToken($request->input('client'))->first()->id,
                "title"=>$request->input('title'),
                "slug"=>null,
                "parent"=>0,
                "excerpt"=>$request->input('excerpt'),
                "postal_code"=>$request->input('postal_code'),
                "mobile"=>$request->input('mobile'),
                "city"=>$request->input('city'),
                "address"=>$request->input('address'),
                "status"=>$request->input('status'),
                "special"=>$request->input('special'),
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                "text"=>$request->input('text'),
               
            ]);
            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!isNot($request->input('price'))){
                $this->entity->price()->update([
                    'amount'=>$request->input('price'),
                ]);
            }
            else{
                $this->entity->price()->create([
                    'amount'=>0,
                ]);
            }

         


          


            $this->entity->syncTags([]);

            if($request->has('attributes')){
                $this->entity->syncAttribute([]);
            }

            if($request->has('properties')) {
                $this->entity->syncProperty([]);
            }

            if(!$updated){
                DB::rollBack();
                return redirect()->back()->with('error',__('product::products.error'));
            }else{
                DB::commit();
                return redirect(route("products.index"))->with('message',__('product::products.update'));
            }


        }catch (\Exception $exception){
         
            DB::rollBack();
            return redirect()->back()->with('error',__('product::products.error'));

        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return void
     */
    public function destroy($token)
    {
        try {
            DB::beginTransaction();
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            if($this->entity->Hasmedia(config('cms.collection-image'))){
                destroyMedia($this->entity,config('cms.collection-image'));
            }
            if($this->entity->Hasmedia(config('cms.collection-images'))){
                destroyMedia($this->entity,config('cms.collection-images'));
            }
            $deleted=$this->entity->delete();

            if(!$deleted){
                DB::rollBack();
                return redirect()->back()->with('error',__('product::products.error'));
            }else{
                DB::commit();
                return redirect(route("products.index"))->with('message',__('product::products.delete'));
            }

        }catch (\Exception $exception){
            DB::rollBack();
           
            return abort('500');
        }
    }

     /**
     * Remove the specified resource from storage.
     * @param $token
     * @return void
     */
    public function approved($token)
    {
        try {
            DB::beginTransaction();
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            
            $client=Client::find($this->entity->user);
           

            if( $this->entity->status!='2'){
                $this->entity->update([
                    'status'=>2,
                ]);
                $result=[
                 
                    'email'=>$client->email,
                    'firstname'=>$client->first_name,
                    'title'=>'Zugelassen',

                ];

                sendNotificationCustomEmail($result,'emails.front.approve-product'); 
            
           
          
                DB::commit();
                return redirect()->back()->with('message',__('product::products.store'));
            }
            else{
                DB::rollBack();
           
            return redirect()->back()->with('error',__('product::products.error'));
            }
               
            

        }catch (\Exception $exception){
            DB::rollBack();
           
            return redirect()->back()->with('error',__('product::products.error'));

        }
    }

}
