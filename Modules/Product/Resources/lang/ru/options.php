<?php
return [
    "text-create"=>"you can create your option",
    "text-edit"=>"you can edit your option",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"options list",
    "singular"=>"option",
    "collect"=>"options",
    "permission"=>[
        "product-full-access"=>"options full access",
        "product-list"=>"options list",
        "product-delete"=>"option delete",
        "product-create"=>"option create",
        "product-edit"=>"edit option",
    ]
];
