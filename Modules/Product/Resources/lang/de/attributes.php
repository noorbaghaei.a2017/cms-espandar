<?php
return [
    "text-create"=>"you can create your attribute",
    "text-edit"=>"you can edit your attribute",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"attributes list",
    "singular"=>"attribute",
    "collect"=>"attributes",
    "permission"=>[
        "product-full-access"=>"attributes full access",
        "product-list"=>"attributes list",
        "product-delete"=>"attribute delete",
        "product-create"=>"attribute create",
        "product-edit"=>"edit attribute",
    ]
];
