<?php

return [
    "text-create"=>' با استفاده از فرم زیر میتوانید ویژگی جدید اضافه کنید.',
    "text-edit"=>' با استفاده از فرم زیر میتوانید ویژگی خود را ویرایش کنید.',
    "store"=>"ثبت با موفقیت انجام شد.",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست ویژگی ها",
    "singular"=>"ویژگی",
    "collect"=>"ویژگی ها",
    "permission"=>[
        "product-full-access"=>"دسترسی کامل به ویژگی ها",
        "product-list"=>"لیست ویژگی ها",
        "product-delete"=>"ویرایش ویژگی",
        "product-create"=>"ایجاد ویژگی",
        "product-edit"=>"ویرایش ویژگی",
    ]
];
