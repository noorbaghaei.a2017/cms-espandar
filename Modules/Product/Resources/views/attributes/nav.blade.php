<li>
        <a href="{{route('attributes.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('product.icons.attribute')}}"> </i>
                              </span>
            <span class="nav-text">{{__('product::attributes.collect')}}</span>
        </a>
    </li>
